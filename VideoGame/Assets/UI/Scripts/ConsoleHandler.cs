﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleHandler : MonoBehaviour, IDesactivable
{

    private bool showed = false;
    public bool Showed { get { return this.showed; } }

    [SerializeField]
    private GameObject scrollBar;

    [HideInInspector]
    public ConsoleManager consoleManager;

    [SerializeField]
    private GameObject textObj;

    [SerializeField]
    private GameObject inputFieldObj;
    private InputField inputField;

    [SerializeField]
    private GameObject texts;
    [SerializeField]
    private int maxTexts;

    [SerializeField]
    private GameObject chatObj;
    private Image chatImg;

    private GameObject currentObj;
    private bool activated = false;

	// Use this for initialization
	void Start ()
    {
        //consoleManager = consoleManagerObj.GetComponent<ConsoleManager>();
        inputField = inputFieldObj.GetComponent<InputField>();
        chatImg = chatObj.GetComponent<Image>();
        Desactivate();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 pos = texts.transform.localPosition;
        float height = ((RectTransform)texts.transform).sizeDelta.y;
        float heightContent = ((RectTransform)texts.transform.parent).sizeDelta.y;
        texts.transform.localPosition = new Vector3(pos.x, height * (1f/2f) - heightContent, pos.z);

        if (activated && Input.GetKeyDown(KeyCode.Return))
            HandleInput();
    }

    public void Activate(GameObject obj)
    {
        currentObj = obj;
        activated = true;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        scrollBar.GetComponent<Image>().enabled = true;
        scrollBar.GetComponent<Scrollbar>().enabled = true;
        scrollBar.transform.GetChild(0).GetChild(0).GetComponent<Image>().enabled = true;

        for (int i = 0; i < texts.transform.childCount; i++)
            texts.transform.GetChild(i).GetComponent<TextConsole>().LockActive();

        chatImg.enabled = true;
        inputField.gameObject.SetActive(true);
        ActiveGUI.set.Add(this);
    }

    public void Desactivate()
    {
        scrollBar.GetComponent<Image>().enabled = false;
        activated = false;
        scrollBar.GetComponent<Scrollbar>().enabled = false;
        scrollBar.transform.GetChild(0).GetChild(0).GetComponent<Image>().enabled = false;

        for (int i = 0; i < texts.transform.childCount; i++)
            texts.transform.GetChild(i).GetComponent<TextConsole>().UnlockActive();

        chatImg.enabled = false;
        inputField.gameObject.SetActive(false);
        ActiveGUI.set.Remove(this);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        inputField.text = "";
        

        if (currentObj != null)
        {
            currentObj.GetComponent<Player>().StartMove();
            currentObj.GetComponent<FirstPersonController>().GetMouseLook().SetCursorLock(true);
            currentObj = null;
        }
    }

    public void HandleInput()
    {
        string input = inputField.text;
        inputField.text = "";

        // TODO: Play sound ?

        consoleManager.HandleInput(input);
    }

    public void AddTextLine(string str)
    {
        GameObject text = Instantiate(textObj);
        text.GetComponent<TextConsole>().SetText(str);

        if (activated)
            text.GetComponent<TextConsole>().LockActive();

        text.transform.SetParent(texts.transform, true);

        if (texts.transform.childCount > 30)
            Destroy(texts.transform.GetChild(0).gameObject);

        text.transform.localPosition = new Vector3(0f, 0f, 0f); //.Set(0f, 0f, 0f);
        text.transform.localScale = new Vector3(1f, 1f, 1f);
        text.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        scrollBar.GetComponent<Scrollbar>().value = 0f;
    }
}
