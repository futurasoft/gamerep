﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextConsole : MonoBehaviour {

    [SerializeField]
    private float timeToFade;
    [SerializeField]
    private GameObject textObj;
    
    private float time;
    private bool lockedActive;

	// Use this for initialization
	void Start ()
    {
        time = timeToFade;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Material mat = GetComponent<Image>().material;
        if (lockedActive)
        {
            gameObject.SetActive(true);
            return;
        }

        time -= Time.deltaTime;
        time = Mathf.Max(0, time);

        if (time == 0)
            gameObject.SetActive(false);
	}

    public void LockActive()
    {
        lockedActive = true;
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
    }

    public void UnlockActive()
    {
        lockedActive = false;
    }

    public void SetText(string text)
    {
        textObj.GetComponent<Text>().text = text;
    }
}
