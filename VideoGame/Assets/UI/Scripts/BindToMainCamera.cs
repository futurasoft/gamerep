﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BindToMainCamera : MonoBehaviour
{
    private Canvas canvas;

	// Use this for initialization
	void Start ()
    {
        canvas = GetComponent<Canvas>();
	}
	
	// Update is called once per frame
	void Update () {
        canvas.worldCamera = Camera.main;
	}
}
