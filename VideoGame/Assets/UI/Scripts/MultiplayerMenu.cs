﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class MultiplayerMenu : Menu
{

    private List<MatchInfoSnapshot> matchList = new List<MatchInfoSnapshot>();
    private NetManager networkManager;

    private MatchInfoSnapshot selectedMatch = null;

    [SerializeField]
    private GameObject popupJoin;
    [SerializeField]
    private GameObject popupHost;
    [SerializeField]
    private GameObject serverObj1;
    [SerializeField]
    private GameObject serverObj2;
    [SerializeField]
    private GameObject serverList;
    [SerializeField]
    private GameObject selectedMap;
    [SerializeField]
    private GameObject mapName;
    [SerializeField]
    private GameObject loadingMenu;

    void Awake()
	{
		networkManager = (NetManager) NetworkManager.singleton;
	}

    void Start()
    {
        if (networkManager.matchMaker == null)
            networkManager.StartMatchMaker();

        Transform control = gameObject.transform.FindChild("Server Control");
        control.FindChild("Refresh").GetComponent<Button>().onClick.AddListener(Refresh);
        control.FindChild("Host").GetComponent<Button>().onClick.AddListener(Host);
        control.FindChild("Join").GetComponent<Button>().onClick.AddListener(Join);

        Refresh();
    }

	public void StartSolo()
	{
		if (networkManager == null)
			networkManager = (NetManager) NetworkManager.singleton;

        loadingMenu.SetActive(true);
        networkManager.isSolo = true;
		networkManager.StartHost();
	}

	public void UpdateOnlineScene()
	{
		if (networkManager == null)
			networkManager = (NetManager) NetworkManager.singleton;
		
		networkManager.onlineScene = "Maps/Map " + (selectedMap.GetComponent<SelectedMap>().Map + 1);
	}

    public override void Activate()
    {
        string text = "";

        switch (selectedMap.GetComponent<SelectedMap>().Map)
        {
            case 0:
                text = "Level 1";
                break;
            case 1:
                text = "Level 2";
                break;
            case 2:
                text = "Level 3";
                break;
            case 3:
                text = "Level 4";
                break;
            default:
                text = "Invalide";
                break;
        }
        mapName.GetComponent<Text>().text = text.ToUpper();
        base.Activate();
    }

    public void SetMap(int map)
    {
        selectedMap.GetComponent<SelectedMap>().Map = map;
    }

    private void DestroyChildren(GameObject obj)
    {
        foreach (Transform children in obj.transform)
            Destroy(children.gameObject);
            
    }

    private void Refresh()
    {
        networkManager.matchMaker.ListMatches(0, 20, "", false, selectedMap.GetComponent<SelectedMap>().Map, 0, OnMatchList);
    }

    private void Join()
    {
        if (selectedMatch == null)
            return;

        if (!selectedMatch.isPrivate)
        {
            networkManager.matchMaker.JoinMatch(selectedMatch.networkId, "", "", "", selectedMap.GetComponent<SelectedMap>().Map, 0, OnMatchJoined);
            return;
        }

        GameObject popup = Instantiate(popupJoin);

        popup.transform.FindChild("Return")
            .GetComponent<Button>().onClick.AddListener(
            () =>
            {
                Destroy(popup);
            });

        popup.transform.FindChild("Join")
           .GetComponent<Button>().onClick.AddListener(
           () =>
           {
               String password = popup.transform.FindChild("Password").GetComponent<InputField>().text;

               if (password.Length == 0)
                   return;

               networkManager.matchMaker.JoinMatch(selectedMatch.networkId, password, "", "", 0, 0, OnMatchJoined);

           });

        popup.transform.SetParent(gameObject.transform);
        popup.transform.localPosition = new Vector3(0f, 0f, 0f);
        popup.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    private void OnMatchJoined(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        loadingMenu.SetActive(true);
        gameObject.SetActive(false);
        networkManager.OnMatchJoined(success, extendedInfo, matchInfo);
    }

    private void Host()
    {
        GameObject popup = Instantiate(popupHost);

        popup.transform.FindChild("Return")
            .GetComponent<Button>().onClick.AddListener(
            () =>
            {
                Destroy(popup);
            });

         popup.transform.FindChild("Create")
            .GetComponent<Button>().onClick.AddListener(
            () =>
            {
                String name = popup.transform.FindChild("Name").GetComponent<InputField>().text;
                String password = popup.transform.FindChild("Password").GetComponent<InputField>().text;
                name = name.Trim();

                if (name.Length == 0)
                    return;
                
                networkManager.matchMaker.CreateMatch(name, 2, true, password, "", "", selectedMap.GetComponent<SelectedMap>().Map, 0, OnMatchCreate);

            });

        popup.transform.SetParent(gameObject.transform);
        popup.transform.localPosition = new Vector3(0f, 0f, 0f);
        popup.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    private void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        loadingMenu.SetActive(true);
        gameObject.SetActive(false);
        networkManager.OnMatchCreate(success, extendedInfo, matchInfo);
    }

    public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success && matches != null)
        {
            DestroyChildren(serverList);
            matchList = matches;
            float height = ((RectTransform)serverObj1.transform).rect.height;
            float y = -height / 2;
            bool pair = true;

            foreach (MatchInfoSnapshot match in matchList)
            {
                if (match.currentSize == match.maxSize)
                    continue;

                GameObject server = Instantiate(pair ? serverObj1 : serverObj2);
                pair = !pair;
                Button button = server.GetComponent<Button>();

                button.onClick.AddListener(
                    () =>
                    {
                        if (selectedMatch == match)
                        {
                            Join();
                            EventSystem.current.SetSelectedGameObject(null);
                            return;
                        }

                        EventSystem.current.SetSelectedGameObject(button.gameObject);
                        selectedMatch = match;
                    }
                );
                server.transform.FindChild("Locked").gameObject.SetActive(match.isPrivate);
                server.transform.FindChild("Name").GetComponent<Text>().text = match.name;

                server.transform.SetParent(serverList.transform, false);
                server.transform.localPosition.Set(0, y, 0);
                //server.transform.localScale = new Vector3(1f, 1f, 1f);

                y -= height;
            }
        }
        else if (!success)
        {
            Debug.LogError("List match failed: " + extendedInfo);
        }
    }


    public void OnConnected(NetworkMessage msg)
    {
        Debug.Log("Connected!");
    }
	
}
