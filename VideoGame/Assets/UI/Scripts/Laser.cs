﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class Laser : MonoBehaviour
{

    public static bool Rotating { get; set; }

    private UILineRenderer line;

    [SerializeField]
    private int layer;
    [SerializeField]
    private GameObject lineObj;
    private Receptor lastReceptor = null;

    static Laser ()
    {
        Rotating = false;
    }

	void Start ()
    {
        if (lineObj == null)
            Debug.Log(gameObject);
        line = lineObj.GetComponent<UILineRenderer>();
        Vector3 pos = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
        line.Points[0] = pos;
        line.Points[1] = pos;

    }

	// Update is called once per frame
	void Update ()
    {
        Quaternion rotation = transform.parent.parent.localRotation;
        Vector2 origin = rotation * new Vector2(transform.localPosition.x, transform.localPosition.y);
        Vector2 dest;

        bool update = false;

        if (origin != line.Points[0])
        {
            line.Points[0] = origin;
            update = true;
        }

        if (lastReceptor != null && lastReceptor.ActiveLaser == this)
            lastReceptor.ActiveLaser = null;
            
        
        if (Rotating)
        {
            //line.SetPosition(1, transform.position);
            if (line.Points[1] != line.Points[0])
                update = true;
            line.Points[1] = line.Points[0];
            UpdateLine(update);
            return;
        }

        var layerMask = 1 << layer;
        RaycastHit hit;

        Ray ray = new Ray(transform.position, -transform.up);

        if (!Physics.Raycast(ray, out hit, 9000f, layerMask))
        {
            //line.SetPosition(1, transform.position);
            if (line.Points[1] != line.Points[0])
                update = true;
            line.Points[1] = line.Points[0];
            UpdateLine(update);
            return;
        }

        Receptor receptor = hit.transform.GetComponent<Receptor>();

        dest = hit.transform.parent.parent.localRotation * (receptor != null ? new Vector3(hit.transform.parent.localPosition.x, hit.transform.parent.localPosition.y, hit.transform.parent.localPosition.z) :
            new Vector3(hit.transform.localPosition.x, hit.transform.localPosition.y, hit.transform.localPosition.z));
        //line.SetPosition(1, pos);


        

        if (line.Points[1] != dest)
            update = true;
        line.Points[1] = dest;
        UpdateLine(update);

        if (receptor == null)
            return;

        receptor.ActiveLaser = this;
        lastReceptor = receptor;

    }

    private void UpdateLine(bool update)
    {
        if (update)
            line.SetAllDirty();
    }
}
