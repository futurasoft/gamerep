﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class Waiting : MonoBehaviour
{

    public bool Active { get; private set; }

    void Start()
    {
        Active = false;
    }

    public void Enable (GameObject cam)
    {
        Active = true;
        gameObject.SetActive(true);
        cam.GetComponent<BlurOptimized>().enabled = true;
    }

    public void Disable(GameObject cam)
    {
        Active = false;
        gameObject.SetActive(false);
        if (ActiveGUI.set.Count == 0 || !(ActiveGUI.set.First() is PauseMenu))
            cam.GetComponent<BlurOptimized>().enabled = false;
    }
}
