﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateLevels : MonoBehaviour
{
	
	// Update is called once per frame
	void Update ()
    {
        int levelMax = Mathf.Min(((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.Level)).GetValue(), 3) + 1;

        for (int i = 0; i < levelMax; i++)
            transform.GetChild(i).GetComponent<Button>().interactable = true;

        for (int i = levelMax; i < 4; i++)
            transform.GetChild(i).GetComponent<Button>().interactable = false;

    }
}
