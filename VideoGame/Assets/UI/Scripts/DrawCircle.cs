﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI.Extensions;

public class DrawCircle : MonoBehaviour
{

    [SerializeField]
    private GameObject border;
    private UILineRenderer line;

	// Use this for initialization
	void Start ()
    {
        line = border.GetComponent<UILineRenderer>();
        List<Vector3> circle = new List<Vector3>();

        float radius = ((RectTransform)transform).rect.height / 2f - 15f;

        for (float a = 0f; a <= 2*Mathf.PI; a += Mathf.PI/64f)
            circle.Add(new Vector3(radius * Mathf.Cos(a), radius * Mathf.Sin(a), 0f));

        line.Points = new Vector2[circle.Count];

        for (int i = 0; i < circle.Count; i++)
            line.Points[i] = circle[i];
	}
	
}
