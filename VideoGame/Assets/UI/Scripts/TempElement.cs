﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempElement : MonoBehaviour
{
    [SerializeField]
    private float time = 1f;

    private float currTime;

    void Start()
    {
        currTime = time;
    }

    void Update ()
    {
        currTime -= Time.deltaTime;
        currTime = Mathf.Max(currTime, 0f);

        if (currTime == 0f)
        {
            gameObject.SetActive(false);
            currTime = time;
        }
	}
}
