﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Receptor : MonoBehaviour
{

    [SerializeField]
    private Color redColor;
    [SerializeField]
    private Color greenColor;

    public Laser ActiveLaser { get; set; }
    [SerializeField]
    private GameObject interior;
    private Image img;

    private static List<Receptor> receptors;

    static Receptor()
    {
        receptors = new List<Receptor>();
    }

    public static bool IsWinning()
    {
        return receptors.Count != 0 && receptors.Find(receptor => receptor.ActiveLaser == null) == null;
    }

    public static void Clear()
    {
        receptors.Clear();
    }

	// Use this for initialization
	void Start ()
    {
        ActiveLaser = null;
        img = interior.GetComponent<Image>();
        receptors.Add(this);

	}
	
	// Update is called once per frame
	void Update () {
        if (ActiveLaser != null)
            img.color = greenColor;
        else
            img.color = redColor;
	}
}
