﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingManager : MonoBehaviour
{
    [SerializeField]
    private Transform[] rings;
    private Transform currentRing = null;
    private float targetAngle = 0f;
    private float speed = 100f;
    [SerializeField]
    private Sprite blockerSprite;
    [SerializeField]
    private Sprite blockerSelectedSprite;
    [SerializeField]
    private Sprite senderSprite;
    [SerializeField]
    private Sprite senderSelectedSprite;
    [SerializeField]
    private Sprite ringSprite;
    [SerializeField]
    private Sprite ringSelectedSprite;

    void Start ()
    {
        foreach  (Transform ring in rings)
        {
            GameObject border = ring.FindChild("Border").gameObject;
            Transform sender = ring.FindChild("Sender");
            Transform blocker = ring.FindChild("Blocker");
            Transform buttons = ring.FindChild("Buttons");

            UnityEngine.Events.UnityAction click = 
                () =>
                {
                    if (currentRing != null)
                    {
                        GameObject borderCurr = currentRing.FindChild("Border").gameObject;
                        Transform senderCurr = currentRing.FindChild("Sender");
                        Transform blockerCurr = currentRing.FindChild("Blocker");

                        borderCurr.GetComponent<Image>().sprite = ringSprite;

                        for (int i = 0; i < senderCurr.childCount; i++)
                            senderCurr.GetChild(i).GetComponent<Image>().sprite = senderSprite;
                        for (int i = 0; i < blockerCurr.childCount; i++)
                            blockerCurr.GetChild(i).GetComponent<Image>().sprite = blockerSprite;
                    }

                    currentRing = ring;

                    border.GetComponent<Image>().sprite = ringSelectedSprite;

                    for (int i = 0; i < sender.childCount; i++)
                        sender.GetChild(i).GetComponent<Image>().sprite = senderSelectedSprite;
                    for (int i = 0; i < blocker.childCount; i++)
                        blocker.GetChild(i).GetComponent<Image>().sprite = blockerSelectedSprite;
                };

            for (int i = 0; i < buttons.childCount; i++)
            {
                if (buttons.GetChild(i).gameObject.activeInHierarchy)
                {
                    buttons.GetChild(i).GetComponent<Button>().onClick.AddListener(click);
                }
            }
        }

	}
	
	void Update ()
    {
        if (currentRing == null)
        {
            Laser.Rotating = false;
            return;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
            targetAngle += 30f;
        else if (Input.GetKeyDown(KeyCode.UpArrow))
            targetAngle += -30f;

        if (targetAngle != 0)
            Laser.Rotating = true;
        else
            Laser.Rotating = false;

        float rotate = 0f; 
        if (targetAngle > 0)
        {
            rotate = Math.Min(targetAngle, speed * Time.deltaTime);
            targetAngle -= rotate;
        }
        else
        {
            rotate = Math.Max(targetAngle, -speed * Time.deltaTime);
            targetAngle -= rotate;
        }

        currentRing.Rotate(rotate * Vector3.forward);
    }
}
