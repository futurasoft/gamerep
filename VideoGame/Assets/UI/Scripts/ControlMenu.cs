﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlMenu : Menu
{

    [SerializeField]
    private GameObject prefab;
    private Button currentButton = null;
    private Inputs? current = null;

    void Start()
    {
        float height = ((RectTransform)prefab.transform).rect.height;
        float y = -height / 2;

        foreach (KeyValuePair<Inputs, KeyCode> input in MainGame.GetInputManager().GetInputs())
        {
            string name = input.Key.ToString().Replace("_", " ");
            string key = input.Value.ToString();

            GameObject control = Instantiate(prefab);
            Button button = control.GetComponentInChildren<Button>();
            control.transform.FindChild("Control Text").GetComponentInChildren<Text>().text = name + ":";
            button.GetComponentInChildren<Text>().text = key.ToUpper();
            button.onClick.AddListener(
                () =>
                {
                    OnButtonClick(button, input.Key);
                }
            );

            control.transform.SetParent(gameObject.transform, false);
            control.transform.localPosition = new Vector3(656.5f, y, 0);
            control.transform.localScale = new Vector3(1f, 1f, 1f);

            y -= height;
        }

        RectTransform rt = GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, -y - height / 2);
    }

    void Update()
    {
        if (!Input.anyKeyDown || current == null)
            return;

        KeyCode? key = GetKeyDown();

        if (key == null)
            return;

        OnClick((KeyCode) key);
    }

    private void OnButtonClick(Button button, Inputs input)
    {
        if (current != null)
            return;

        current = input;
        currentButton = button;
    }

    private void OnClick(KeyCode key)
    {
        MainGame.GetInputManager().SetButton((Inputs) current, key);
        currentButton.GetComponentInChildren<Text>().text = key.ToString().ToUpper();
        MainGame.GetInputManager().Save();

        current = null;
        currentButton = null;
    }

    private KeyCode? GetKeyDown()
    {
        foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
            if (Input.GetKeyDown(key))
                return key;

        return null;
    }
}
