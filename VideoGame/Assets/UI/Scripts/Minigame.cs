﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minigame : MonoBehaviour, IDesactivable, Resetable
{

    [SerializeField]
    private bool isLevel1 = false;
    [SerializeField]
    private VoiceoverTrigger triggerStart;
    [SerializeField]
    private VoiceoverTrigger triggerEnd;

    private bool hasTriggerStart = false;
    private bool hasTriggerEnd = false;

    public void Activate()
    {
        ActiveGUI.set.Add(this);
        gameObject.SetActive(true);
        Player.LocalPlayer.StopMove = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        if (isLevel1 && !hasTriggerStart)
        {
            triggerStart.Trigger();
            hasTriggerStart = true;
        }
            
    }

    public void Desactivate()
    {
        ActiveGUI.set.Remove(this);
        gameObject.SetActive(false);
        Player.LocalPlayer.StartMove();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (isLevel1 && Receptor.IsWinning() && !hasTriggerEnd)
        {
            triggerEnd.Trigger();
            hasTriggerEnd = true;
        }
            
    }

    public void Reset()
    {
        hasTriggerEnd = false;
        hasTriggerStart = false;
    }

    private void Start()
    {
        ResetableUtils.InitReset(this);
    }
}
