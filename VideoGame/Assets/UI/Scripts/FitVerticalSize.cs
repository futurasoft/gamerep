﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitVerticalSize : MonoBehaviour
{

    [SerializeField]
    private float additionnalHeight;

	void Update ()
    {
        RectTransform rectTransform = ((RectTransform)transform);
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, ((RectTransform)transform.GetChild(0)).sizeDelta.y + additionnalHeight);
	}
}
