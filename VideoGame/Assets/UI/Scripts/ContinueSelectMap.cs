﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueSelectMap : MonoBehaviour {

    [SerializeField]
    private GameObject selectedMap;

    public void UpdateMap()
    {
        selectedMap.GetComponent<SelectedMap>().Map = ((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.Level)).GetValue();
    }

}
