﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{

    [SerializeField]
    private GameObject background = null, menu = null;

    public virtual void Activate()
    {
        menu.SetActive(true);
        if (background != null)
            background.SetActive(true);
    }

    public void Desactivate()
    {
        menu.SetActive(false);
        if (background != null)
            background.SetActive(false);
    }
}
