﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedMap : MonoBehaviour {

    private int map;
    public int Map { get { return this.map; } set {
            this.map = value;
            Player.HaveGunDefault = value > 1;
            Player.HaveCrackerDefault = value > 2;
        } }

}
