﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.ImageEffects;

public class PauseMenu : MonoBehaviour, IDesactivable
{

    [SerializeField]
    private GameObject menu;

    [SerializeField]
    private GameObject options;

    [SerializeField]
    private GameObject settings;

    [SerializeField]
    private GameObject controls;

    private GameObject currentObj = null;
    private GameObject currentCam = null;

    [SerializeField]
    private GameObject waiting;

    public void BackToMenu()
    {
        ((NetManager)NetworkManager.singleton).Disconnect();
    }

    public void Activate(GameObject obj, GameObject cam)
    {
        currentObj = obj;
        currentCam = cam;

        ActiveGUI.set.Add(this);

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        gameObject.SetActive(true);
        Show();
        HideControls();
        HideOptions();
        HideSettings();
        ShowBlur();
    }

    public void Desactivate()
    {
        gameObject.SetActive(false);
        ActiveGUI.set.Remove(this);


        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (currentCam != null && !waiting.GetComponent<Waiting>().Active)
        {
            HideBlur();
            currentCam = null;
        }
        

        if (currentObj != null && !waiting.GetComponent<Waiting>().Active)
        {
            currentObj.GetComponent<Player>().StartMove();
            currentObj = null;
        }
    }

    private void ShowBlur()
    {
        currentCam.GetComponent<BlurOptimized>().enabled = true;
    }

    private void HideBlur()
    {
        currentCam.GetComponent<BlurOptimized>().enabled = false;
    }

    public void Show()
    {
        menu.SetActive(true);
    }

    public void Hide()
    {
        menu.SetActive(false);
    }

    public void ShowOptions()
    {
        options.SetActive(true);
    }

    public void HideOptions()
    {
        options.SetActive(false);
    }

    public void ShowSettings()
    {
        settings.SetActive(true);
    }

    public void HideSettings()
    {
        settings.SetActive(false);
    }

    public void ShowControls()
    {
        controls.SetActive(true);
    }

    public void HideControls()
    {
        controls.SetActive(false);
    }
}
