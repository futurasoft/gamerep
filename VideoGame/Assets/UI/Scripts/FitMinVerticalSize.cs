﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitMinVerticalSize : MonoBehaviour
{
    private float startHeight;

	void Start ()
    {
        startHeight = ((RectTransform)transform).sizeDelta.y;
    }

    void Update()
    {
        RectTransform rectTransform = ((RectTransform)transform);
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, Mathf.Max(((RectTransform)transform.GetChild(0)).sizeDelta.y, startHeight));
    }
}
