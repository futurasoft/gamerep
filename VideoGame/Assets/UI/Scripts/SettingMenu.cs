﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingMenu : Menu
{

    [SerializeField]
    private GameObject prefabFloat;
    [SerializeField]
    private GameObject prefabInt;
    [SerializeField]
    private GameObject prefabBool;
    [SerializeField]
    private GameObject prefabEnum;

    // Use this for initialization
    void Start () {
        float height = ((RectTransform)prefabInt.transform).rect.height;
        float y = -height / 2;

        foreach (KeyValuePair<Settings, Setting> input in MainGame.GetSettingManager().GetSettings())
        {
            string name = input.Value.GetDisplayName();
            Setting setting = input.Value;

            if (setting.Hidden)
                return;

            GameObject prefab;

            if (setting is SettingBool)
                prefab = prefabBool;
            else if (setting is SettingEnum)
                prefab = prefabEnum;
            else if (setting is SettingFloat)
                prefab = prefabFloat;
            else
                prefab = prefabInt;

            GameObject obj = Instantiate(prefab);
            obj.transform.FindChild("Text").GetComponent<Text>().text = name + ":";

            if (setting is SettingBool)
            {
                Toggle toggle = obj.GetComponentInChildren<Toggle>();
                toggle.isOn = ((SettingBool) setting).GetValue();
                toggle.onValueChanged.AddListener(
                    (val) =>
                    {
                        ((SettingBool)setting).SetValue(val);
                        setting.Save();
                    }
                 );
            }
            else if (setting is SettingEnum)
            {
                Dropdown dropdown = obj.GetComponentInChildren<Dropdown>();
                SettingEnum settingEnum = (SettingEnum) setting;
                dropdown.ClearOptions();

                List<string> strings = new List<string>();

                foreach (Enum val in Enum.GetValues(settingEnum.GetValue().GetType()))
                    strings.Add(val.ToString());

                dropdown.AddOptions(strings);
                dropdown.value = (int) (object) settingEnum.GetValue();
                dropdown.onValueChanged.AddListener(
                    (val) =>
                    {
                        settingEnum.SetValue((Enum)Enum.ToObject(settingEnum.GetValue().GetType(), val));
                        settingEnum.Save();
                    }
                );
            }
            else if (setting is SettingFloat)
            {
                Slider slider = obj.GetComponentInChildren<Slider>();
                SettingFloat settingFloat = (SettingFloat) setting;
                slider.minValue = settingFloat.GetMin();
                slider.maxValue = settingFloat.GetMax();
                slider.value = settingFloat.GetValue();
                slider.GetComponentInChildren<Text>().text = slider.value + "";

                slider.onValueChanged.AddListener(
                    (val) =>
                    {
                        settingFloat.SetValue(val);
                        slider.GetComponentInChildren<Text>().text = Math.Round(val, 2) + "";
                        settingFloat.Save();
                    }
                 );
            }
            else
            {
                Slider slider = obj.GetComponentInChildren<Slider>();
                SettingInt settingInt = (SettingInt)setting;
                slider.minValue = settingInt.GetMin();
                slider.maxValue = settingInt.GetMax();
                slider.value = settingInt.GetValue();
                slider.GetComponentInChildren<Text>().text = slider.value + "";

                slider.onValueChanged.AddListener(
                    (val) =>
                    {
                        settingInt.SetValue((int) val);
                        slider.GetComponentInChildren<Text>().text = val + "";
                        settingInt.Save();
                    }
                 );
            }

            obj.transform.SetParent(gameObject.transform, false);
            obj.transform.localPosition = new Vector3(656.5f, y, 0);
            //obj.transform.localPosition.Set(0, y, 0);
            obj.transform.localScale = new Vector3(1f, 1f, 1f);

            y -= height;
        }
    }
}
