﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        GetComponent<Button>().onClick.AddListener(() => { if (!Application.isEditor) System.Diagnostics.Process.GetCurrentProcess().Kill();  });
	}
}
