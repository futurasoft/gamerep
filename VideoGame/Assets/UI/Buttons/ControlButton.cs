﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlButton : MonoBehaviour
{
    private Button button;
    private Text text;
    private bool clicked = false;

	// Use this for initialization
	void Start ()
	{
        this.button = gameObject.GetComponent<Button>();
	    this.button.onClick.AddListener(() => OnClick());
        this.text = this.button.GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (clicked && Input.anyKey)
        {
            this.text.text = Input.inputString;
        }
	}

    void OnClick()
    {
        if (!clicked)
            clicked = true;
    }
}
