﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class ButtonTextChange : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private Text text;
    [SerializeField]
    private Color regularColor;
    [SerializeField]
    private Color hoverColor;

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.color = hoverColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = regularColor;
    }

    public void ResetColor()
    {
        text.color = regularColor;
    }
}
