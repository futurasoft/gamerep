﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class NetManager : NetworkManager {

    [SerializeField]
    private GameObject selectedMap;
    public bool isSolo = false;
    public bool isHost = false;

    public bool firstSpawned = false;

    public void Disconnect()
    {
        isSolo = false;
        isHost = false;
        firstSpawned = false;
        if (matchMaker != null && matchInfo != null)
            matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, OnDropConnection);
        StopHost();
    }

    public override void OnStopHost()
    {
        isSolo = false;
        isHost = false;
        firstSpawned = false;
        base.OnStopHost();
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        ActiveGUI.set.Clear();
        ResetableUtils.Clear();
        Receptor.Clear();
        Player.HaveCracker = Player.HaveCrackerDefault;
        Player.HaveGun = Player.HaveGunDefault;
        Player.HaveCard = false;
    }

    public override void OnStartHost()
    {
        isHost = true;
        base.OnStartHost();
    }

}
