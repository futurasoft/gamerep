﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ConvertCube : MonoBehaviour {

    public GameObject newCube;
    public Transform newTransform;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < transform.childCount; i++)
        {
            Transform trans = transform.GetChild(i);
            if (trans.name.StartsWith("Cube"))
            {
                GameObject cube = Instantiate(newCube, trans.position, trans.rotation, newTransform);
                cube.transform.localScale = trans.localScale;
                cube.transform.localScale.Set(cube.transform.localScale.x, 190f, cube.transform.localScale.z);
                cube.transform.position.Set(cube.transform.position.x, 26.5f, cube.transform.position.z);
                DestroyImmediate(trans.gameObject);
                i--;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
    }
}
