﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class VoiceoverTrigger : NetworkBehaviour {

    [SerializeField]
    public AudioClip clip;
    [SerializeField]
    public string[] sentences;
    [SerializeField]
    public float[] timings;
    [SerializeField]
    public VoiceoverManager manager;
    [SerializeField]
    public float endTime;

    public void Trigger()
    {
        manager.Trigger(gameObject);
    }

    public void TriggerLocal()
    {
        manager.TriggerLocal(gameObject);
    }
}
