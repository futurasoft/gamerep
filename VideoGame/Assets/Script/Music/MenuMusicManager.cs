﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusicManager : MonoBehaviour {
    
    [SerializeField]
    private AudioSource audiosource;

    [SerializeField]
    private AudioClip introTrack;
    [SerializeField]
    private AudioClip loopTrack;

    private float volume, mastervolume;

    void Start () {
        audiosource.loop = false;
        audiosource.clip = introTrack;
        audiosource.Play();
        StartCoroutine(PlayDelayedLoop());
	}

    IEnumerator PlayDelayedLoop()
    {
        yield return new WaitForSeconds(audiosource.clip.length);
        audiosource.clip = loopTrack;
        audiosource.loop = true;
        audiosource.Play();
    }
	
    public void Stop()
    {
        audiosource.Pause();
    }

	void Update ()
    {
        
        volume = (float)((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.MusicVolume)).GetValue()/100.0f;
        mastervolume = (float)((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.MasterVolume)).GetValue() / 100.0f;
        audiosource.volume = volume * mastervolume;
    }
}
