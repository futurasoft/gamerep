﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    //public static MusicManager Instance { get; set; }
    [SerializeField]
    private AudioSource audiosource;
    [SerializeField]
    private float distance = 25.0f;

    [SerializeField]
    private AudioClip intro;
    [SerializeField]
    private AudioClip afterDropIntro;
    [SerializeField]
    private AudioClip drop;
    [SerializeField]
    private List<AudioClip> loopMusics;
    [SerializeField]
    private List<AudioClip> afterDropLoopMusics;

    private IAPatrolBot[] mobs;
    private CameraAutoRotationScript[] cameras;
    private Player player;

    private float virtualVolume, volume, mastervolume;
    private bool afterDrop, haveCardSecurity;

    int cameraNb, mobNb;

    void Start()
    {
        mobs = FindObjectsOfType<IAPatrolBot>();
        cameras = FindObjectsOfType<CameraAutoRotationScript>();
        player = Player.LocalPlayer;

        afterDrop = false;
        virtualVolume = 1.0f;
        audiosource.clip = intro;
        audiosource.loop = false;
        audiosource.Play();
        haveCardSecurity = false;
        StartCoroutine(CardSecurity());
    }

    IEnumerator CardSecurity()
    {
        yield return new WaitForSeconds(3.0f);
        haveCardSecurity = true;
    }

    void Update()
    {
        if (Player.HaveCard && !afterDrop && haveCardSecurity)
        {
            Drop();
        }
        mobs = FindObjectsOfType<IAPatrolBot>();
        cameras = FindObjectsOfType<CameraAutoRotationScript>();
        player = Player.LocalPlayer;

        mastervolume = (float)((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.MasterVolume)).GetValue() / 100.0f;
        volume = (float)((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.MusicVolume)).GetValue() / 100.0f;
        if (!audiosource.isPlaying)
        {
            if (loopMusics.Count != 0 && !afterDrop)
            {
                audiosource.clip = loopMusics[Random.Range(0, loopMusics.Count)];
                audiosource.Play();
            }else if (afterDrop && afterDropLoopMusics.Count != 0)
            {
                audiosource.clip = afterDropLoopMusics[Random.Range(0, loopMusics.Count)];
                audiosource.Play();
            }
        }
        if (player != null)
        {
            cameraNb = 0;
            if (cameras.Length != 0)
            {
                for (int i = 0; i < cameras.Length; i++)
                {
                    float dx = Mathf.Abs(cameras[i].transform.position.x - player.transform.position.x);
                    float dy = Mathf.Abs(cameras[i].transform.position.y - player.transform.position.y);
                    float dz = Mathf.Abs(cameras[i].transform.position.z - player.transform.position.z);
                    if (Mathf.Pow(dx, 2f) + Mathf.Pow(dy, 2f) + Mathf.Pow(dz, 2f) < Mathf.Pow(distance, 2f))
                        cameraNb++;
                }
            }
            mobNb = 0;
            if (mobs.Length != 0)
            {
                for (int i = 0; i < mobs.Length; i++)
                {
                    float dx = Mathf.Abs(mobs[i].transform.position.x - player.transform.position.x);
                    float dy = Mathf.Abs(mobs[i].transform.position.y - player.transform.position.y);
                    float dz = Mathf.Abs(mobs[i].transform.position.z - player.transform.position.z);
                    if (Mathf.Pow(dx, 2f) + Mathf.Pow(dy, 2f) + Mathf.Pow(dz, 2f) < Mathf.Pow(distance, 2f))
                        mobNb++;
                }
            }
        }
        virtualVolume = (mobNb + 1.9f) / (mobs.Length + 2.0f) * (cameraNb + 1.0f) / (cameras.Length + 2.0f);
        audiosource.volume = virtualVolume * volume * mastervolume;
    }

    public void Drop()
    {
        audiosource.loop = false;
        audiosource.Pause();
        audiosource.clip = drop;
        audiosource.Play();
        afterDrop = true;

        StartCoroutine(RestartIntro());
    }

    public void Reset()
    {
        afterDrop = false;
        virtualVolume = 1.0f;
        audiosource.clip = intro;
        audiosource.loop = false;
        audiosource.Play();
    }

    IEnumerator RestartIntro()
    {
        yield return new WaitForSeconds(audiosource.clip.length);
        audiosource.Pause();
        audiosource.clip = afterDropIntro;
        audiosource.Play();
    }
}
