﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AutoTexturing : MonoBehaviour {

    [SerializeField]
    private Vector2 ratio = new Vector2(1, 1);
    [SerializeField]
    private Texture2D texture;

    // Use this for initialization
    void Update ()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform trans = transform.GetChild(i);
            if (trans.name.StartsWith("Cube"))
            {
                for (int j = 0; j < 4; j++)
                {
                    /*
                    if (trans.GetChild(j).gameObject.GetComponent<AutoTilingMaterial>() == null)
                        trans.GetChild(j).gameObject.AddComponent<AutoTilingMaterial>();
                    trans.GetChild(j).gameObject.GetComponent<AutoTilingMaterial>().texture = texture;
                    trans.GetChild(j).gameObject.GetComponent<AutoTilingMaterial>().ratio = ratio;
                    */
                    if (trans.GetChild(j).gameObject.GetComponent<AutoTilingMaterial>() != null)
                        trans.GetChild(j).gameObject.GetComponent<AutoTilingMaterial>().enabled = false;
                }
            }
            else
            {
                /*
                if (trans.gameObject.GetComponent<AutoTilingMaterial>() == null)
                    trans.gameObject.AddComponent<AutoTilingMaterial>();
                trans.gameObject.GetComponent<AutoTilingMaterial>().texture = texture;
                trans.gameObject.GetComponent<AutoTilingMaterial>().ratio = ratio;
                */
                if (trans.gameObject.GetComponent<AutoTilingMaterial>() != null)
                    trans.gameObject.GetComponent<AutoTilingMaterial>().enabled = false;
            }
        }
    }
	
	// Update is called once per frame
	void Start () {
		
	}
}
