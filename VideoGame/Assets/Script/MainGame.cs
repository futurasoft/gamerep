﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGame {

    private static SettingManager settings = null;
    private static InputManager input = null;

    public static SettingManager GetSettingManager()
    {
        if (settings == null)
            settings = new SettingManager();

        return settings;
    }

    public static InputManager GetInputManager()
    {
        if (input == null)
            input = new InputManager();

        return input;
    }
}
