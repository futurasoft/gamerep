﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ZoneTriggerSound : NetworkBehaviour, Resetable
{
    [SerializeField]
    private VoiceoverTrigger trigger;

    private bool hasTrigger = false;

    public void Reset()
    {
        hasTrigger = false;
    }

    void Start()
    {
        ResetableUtils.InitReset(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null && isServer && !hasTrigger)
        {
            trigger.Trigger();
            hasTrigger = true;
        }
    }

}
