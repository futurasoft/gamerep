﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Setting
{
    public static readonly string Prefix = "settings";

    private Settings setting;
    private string displayName;
    private bool hidden;

    public bool Hidden { get { return this.hidden; } }

    public Setting(string displayName, Settings setting, bool hidden)
    {
        this.setting = setting;
        this.displayName = displayName;
        this.hidden = hidden;
    }

    public Setting(string displayName, Settings setting)
        : this(displayName, setting, false)
    {}

    public string GetDisplayName()
    {
        return this.displayName;
    }

    public Settings GetSettings()
    {
        return this.setting;
    }

    public abstract void Save();

    public abstract void Load();

}

public class SettingEnum : Setting
{
    private Enum value;

    public SettingEnum(string displayName, Settings setting, Enum defaultValue, bool hidden) 
        : base(displayName, setting, hidden)
    {
        this.value = defaultValue;
    }

    public SettingEnum(string displayName, Settings setting, Enum defaultValue) 
        : this(displayName, setting, defaultValue, false)
    {
    }

    public virtual Enum GetValue()
    {
        return this.value;
    }

    public virtual void SetValue(Enum value)
    {
        if (value.GetType() != this.value.GetType())
            throw new ArgumentException("Enum provided is different !");

        this.value = value;
    }

    public override void Save()
    {
        SettingsHelper.Set(Prefix, GetSettings().ToString(), Convert.ToInt32(this.value));
    }

    public override void Load()
    {
        if (SettingsHelper.Has(Prefix, GetSettings().ToString()))
            this.value = (Enum) Enum.ToObject(value.GetType(), SettingsHelper.GetInt(Prefix, GetSettings().ToString()));
    }
}


public class SettingFloat : Setting
{
    private float value;
    private float min;
    private float max;

    public SettingFloat(string displayName, Settings setting, float defaultValue, float min, float max, bool hidden) 
        : base(displayName, setting, hidden)
    {
        this.value = defaultValue;
        this.min = min;
        this.max = max;
    }

    public SettingFloat(string displayName, Settings setting, float defaultValue, float min, float max) 
        : this(displayName, setting, defaultValue, min, max, false)
    {}

    public void SetValue(float value)
    {
        if (this.value < min || this.value > max)
            throw new IndexOutOfRangeException("The value is not in the allowed range !");
        this.value = value;
    }

    public float GetValue()
    {
        return this.value;
    }

    public override void Save()
    {
        SettingsHelper.Set(Prefix, GetSettings().ToString(), this.value);
    }

    public override void Load()
    {
        if (SettingsHelper.Has(Prefix, GetSettings().ToString()))
            this.value = SettingsHelper.GetFloat(Prefix, GetSettings().ToString());
    }

    public float GetMin()
    {
        return this.min;
    }

    public float GetMax()
    {
        return this.max;
    }
}

public class SettingInt : Setting
{
    private int value;
    private int min;
    private int max;

    public SettingInt(string displayName, Settings setting, int defaultValue, int min, int max, bool hidden) 
        : base(displayName, setting, hidden)
    {
        this.value = defaultValue;
        this.min = min;
        this.max = max;
    }

    public SettingInt(string displayName, Settings setting, int defaultValue, int min, int max)
    : this(displayName, setting, defaultValue, min, max, false)
    {}

    public void SetValue(int value)
    {
        if (this.value < min || this.value > max)
            throw new IndexOutOfRangeException("The value is not in the allowed range !");
        this.value = value;
    }

    public int GetValue()
    {
        return this.value;
    }

    public override void Save()
    {
        SettingsHelper.Set(Prefix, GetSettings().ToString(), this.value);
    }

    public override void Load()
    {
        if (SettingsHelper.Has(Prefix, GetSettings().ToString()))
            this.value = SettingsHelper.GetInt(Prefix, GetSettings().ToString());
    }

    public int GetMin()
    {
        return this.min;
    }

    public int GetMax()
    {
        return this.max;
    }
}

public class SettingBool : Setting
{
    private bool value;

    public SettingBool(string displayName, Settings setting, bool defaultValue, bool hidden) 
        : base(displayName, setting, hidden)
    {
        this.value = defaultValue;
    }

    public SettingBool(string displayName, Settings setting, bool defaultValue) 
        : this(displayName, setting, defaultValue, false)
    {}

    public void SetValue(bool value)
    {
        this.value = value;
    }

    public bool GetValue()
    {
        return this.value;
    }

    public override void Save()
    {
        SettingsHelper.Set(Prefix, GetSettings().ToString(), Convert.ToInt32(this.value));
    }

    public override void Load()
    {
        if (SettingsHelper.Has(Prefix, GetSettings().ToString()))
            this.value = Convert.ToBoolean(SettingsHelper.GetInt(Prefix, GetSettings().ToString()));
    }
}

public class SettingQuality : SettingEnum
{
    public SettingQuality() 
        : base("Qualité graphique", Settings.QualityLevel, QualityLevel.Fast)
    {   
    }

    private bool firstSave = false;

    public override Enum GetValue()
    {
#pragma warning disable CS0618 // Type or member is obsolete
        return QualitySettings.currentLevel;
#pragma warning restore CS0618 // Type or member is obsolete
    }

    public override void Save()
    {
        if (!firstSave)
            SetValue(GetValue());
        else if (base.GetValue() != GetValue())
#pragma warning disable CS0618 // Type or member is obsolete
            QualitySettings.currentLevel = (QualityLevel) base.GetValue();
#pragma warning restore CS0618 // Type or member is obsolete
    }
}