﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ElevatorButton : NetworkBehaviour, Resetable, Interactable
{

    [SerializeField]
    private Collider[] doorColliders;
    [SerializeField]
    private Animator doorAnimator;
    [SerializeField]
    private Animator elevatorAnimator;
    private bool toggled = false;
    [SerializeField]
    private float delay = 1f;
    private float _delay;
    private bool assigned = false;
    [SerializeField]
    private VoiceoverTrigger trigger;
    private float timeout;

    public void OnContinuousInteract()
    {
    }

    public void OnInteract()
    {

        if (Receptor.IsWinning() && !toggled)
        {
            if (isServer)
                RpcInteract();
            else
                CmdInteract();
        }

        if (!Receptor.IsWinning() && timeout == 0f)
        {
            timeout = 5f;
            trigger.TriggerLocal();
        }
    }

    [Command]
    private void CmdInteract()
    {
        RpcInteract();
    }

    [ClientRpc]
    private void RpcInteract()
    {
        toggled = true;
        doorAnimator.SetBool("Open", true);
        elevatorAnimator.SetBool("Open", true);
    }

    public void Reset()
    {
        toggled = false;
        foreach (Collider coll in doorColliders)
            coll.enabled = true;
        doorAnimator.SetBool("Open", false);
        elevatorAnimator.SetBool("Open", false);
        _delay = delay;
        timeout = 0f;
    }

    void Start ()
    {
        ResetableUtils.InitReset(this);
        _delay = delay;
    }
	
	void Update ()
    {
        if (toggled)
        {
            _delay -= Time.deltaTime;
            _delay = Mathf.Max(0, _delay);

            if (_delay == 0f)
                foreach (Collider coll in doorColliders)
                    coll.enabled = false;
        }

        if (timeout != 0f)
        {
            timeout -= Time.deltaTime;
            timeout = Mathf.Max(0f, timeout);
        }

        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }
}
