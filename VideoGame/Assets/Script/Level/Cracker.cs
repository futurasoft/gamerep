﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Cracker : NetworkBehaviour, Interactable, Resetable
{
    private bool assigned = false;

    public void OnContinuousInteract()
    { }

    void Start()
    {
        ResetableUtils.InitReset(this);
    }

    public void OnInteract()
    {
        if (isServer)
            RpcGetCracker();
        else
            CmdGetCracker();
    }

    public void Reset()
    {
        gameObject.SetActive(true);
    }

    [Command]
    private void CmdGetCracker()
    {
        RpcGetCracker();
    }

    [ClientRpc]
    private void RpcGetCracker()
    {
        gameObject.SetActive(false);
        Player.HaveCracker = true;
    }

    private void Update()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }
}
