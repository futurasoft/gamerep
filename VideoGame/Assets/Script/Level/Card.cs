﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Card : NetworkBehaviour, Interactable, Resetable
{
    [SerializeField]
    Vector2 pos = new Vector2(20, 40);
    [SerializeField]
    Vector2 size = new Vector2(60, 20);
    [SerializeField]
    Texture2D card_on_hud;
    private bool assigned = false;

    public void OnContinuousInteract()
    { }

    public void OnInteract()
    {
        if (isServer)
            RpcCard();
        else
            CmdCard();
    }

    [Command]
    private void CmdCard()
    {
        RpcCard();
    }

    [ClientRpc]
    private void RpcCard()
    {
        // add on hud
        Player.HaveCard = true;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    public void Reset()
    {
        // remove from hud
        Player.HaveCard = false;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
    }

    void OnGUI()
    {
        if (Player.HaveCard)
            GUI.DrawTexture(new Rect(pos.x, pos.y, size.x, size.y), card_on_hud);
    }

    // Use this for initialization
    void Start ()
    {
        ResetableUtils.InitReset(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }
}
