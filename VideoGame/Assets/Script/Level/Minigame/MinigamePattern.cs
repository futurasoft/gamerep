﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigamePattern : MonoBehaviour, Interactable, Resetable
{
    GameObject MiniGame;
    [SerializeField]
    private VoiceoverTrigger trigger;
    private float timeout = 5f;

    void Start ()
    {
        MiniGame = GameObject.Find("Canvas").transform.FindChild("MiniGame").gameObject;
        ResetableUtils.InitReset(this);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (timeout != 0f)
        {
            timeout -= Time.deltaTime;
            timeout = Mathf.Max(0f, timeout);
        }
    }

    public void OnInteract()
    {
        if (Player.HaveCard)
            MiniGame.GetComponent<Minigame>().Activate();
        else if (timeout == 0f)
        {
            timeout = 5f;
            trigger.TriggerLocal();
        }

    }

    public void OnContinuousInteract()
    {
    }

    public void Reset()
    {
        MiniGame.GetComponent<Minigame>().Desactivate();
        timeout = 5f;
    }
}
