﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EndTechnology : NetworkBehaviour, Resetable, Interactable
{
    private bool assigned = false;
    [SerializeField]
    private Collider[] colliders;

    public void OnContinuousInteract()
    {
    }

    public void OnInteract()
    {
        if (isServer)
            RpcGetTecho();
        else
            CmdGetTechno();
    }

    [Command]
    private void CmdGetTechno()
    {
        RpcGetTecho();
    }

    [ClientRpc]
    private void RpcGetTecho()
    {
        gameObject.SetActive(false);
        foreach (Collider coll in colliders)
            coll.enabled = false;
    }

    public void Reset()
    {
        gameObject.SetActive(true);
        foreach (Collider coll in colliders)
            coll.enabled = true;
    }

    // Use this for initialization
    void Start ()
    {
        ResetableUtils.InitReset(this);
    }
	
	void Update ()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }
}
