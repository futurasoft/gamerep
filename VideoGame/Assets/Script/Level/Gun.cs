﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Gun : NetworkBehaviour, Interactable, Resetable {

    private bool assigned = false;

    public void OnContinuousInteract()
    {}

    void Start()
    {
        ResetableUtils.InitReset(this);
    }

    public void OnInteract()
    {
        if (isServer)
            RpcGetGun();
        else
            CmdGetGun();
    }

    public void Reset()
    {
        gameObject.SetActive(true);
    }

    [Command]
    private void CmdGetGun()
    {
        RpcGetGun();
    }

    [ClientRpc]
    private void RpcGetGun()
    {
        gameObject.SetActive(false);
        Player.HaveGun = true;
    }

    private void Update()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }
}
