﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTerminal : MonoBehaviour {

    private Transform door;
    private Vector3 position;
    private Vector3 down;
    private Vector3 up;
    private bool open;

    // Use this for initialization
    void Start()
    {
        position = this.transform.position;
        door = this.transform.parent.FindChild("door");
        down = new Vector3(0, -9.5F, 0);
        up = new Vector3(0, 9.5F, 0);
        open = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!open)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void Open()
    {
        if (this.gameObject.GetComponent<TerminalHacking>().IsActivated())
        {
            door.transform.Translate(down);
            open = true;
        }
    }

    private void Close()
    {
        if (!this.gameObject.GetComponent<TerminalHacking>().IsActivated())
        {
            door.transform.Translate(up);
            open = false;
        }
    }

    public void Reset()
    {
        this.transform.position = position;
    }
}
