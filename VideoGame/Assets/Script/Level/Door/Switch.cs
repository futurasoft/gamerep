﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Switch : NetworkBehaviour, Interactable, Resetable
{
    bool use;
    private Animator animator;
    private bool assigned = false;

    // Use this for initialization
    void Start ()
    {
        animator = GetComponent<Animator>();
        use = false;
        ResetableUtils.InitReset(this);
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }

    void Interactable.OnInteract()
    {
        if (isServer)
            RpcOnInteract();
        else
            CmdOnInteract();
    }

    [Command]
    private void CmdOnInteract()
    {
        RpcOnInteract();
    }

    [Command]
    public void CmdAssign()
    {
        GetComponent<NetworkIdentity>().AssignClientAuthority(connectionToClient);
    }


    [ClientRpc]
    private void RpcOnInteract()
    {
        use = !use;
        animator.SetBool("On", use);

    }

    void Interactable.OnContinuousInteract()
    {
    }

    public bool returnUse()
    {
        return use;
    }

    public void Reset()
    {
        use = false;
    }
}
