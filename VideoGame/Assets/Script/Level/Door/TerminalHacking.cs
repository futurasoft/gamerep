﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Timers;

public class TerminalHacking : NetworkBehaviour, Interactable, Resetable
{
    private float progress = 0.0f;
    [SerializeField]
    private float speed = 0.5f;
    [SerializeField]
    private Vector2 pos = new Vector2(20, 40);
    [SerializeField]
    private Vector2 size = new Vector2(60, 20);
    [SerializeField]
    private Texture2D progressBarEmpty;
    [SerializeField]
    private Texture2D progressBarFull;
    private DateTime lastInteract;
    [SerializeField]
    private int activatedTime = 10000;
    private bool activated = false;
    private Timer chrono;
    private bool assigned = false;
    private float timeout;
    [SerializeField]
    private VoiceoverTrigger trigger;
    private bool firstActivated = false;
    [SerializeField]
    private VoiceoverTrigger triggerSuccess;

    public void OnContinuousInteract()
    {
        if (activated)
            return;
        if (!Player.HaveCracker)
            return;

        if (progress < 1f)
            progress += Time.deltaTime * speed;
        else
        {
            if (isServer)
                RpcActivate();
            else
                CmdActivate();
        }

        lastInteract = DateTime.Now;
    }


    public void OnInteract()
    {
        if (activated)
            return;

        if (!Player.HaveCracker && timeout == 0f)
        {
            timeout = 5f;
            if (trigger != null)
                trigger.TriggerLocal();
            return;
        }

        if (lastInteract.AddSeconds(5) < DateTime.Now)
            progress = 0f;
    }

    [Command]
    private void CmdActivate()
    {
        RpcActivate();
    }

    [ClientRpc]
    private void RpcActivate()
    {
        progress = 0f;
        activated = true;
        chrono.Interval = activatedTime;
        chrono.Enabled = true;
        if (!firstActivated)
        {
            firstActivated = true;
            if (triggerSuccess != null)
                triggerSuccess.TriggerLocal();
        }
    }

    // Use this for initialization
    void Start () {
        lastInteract = DateTime.Now.AddHours(-1);
        chrono = new Timer(activatedTime);
        chrono.Elapsed += resetUse;
        chrono.AutoReset = false;
        ResetableUtils.InitReset(this);
    }

    void resetUse(object Source, ElapsedEventArgs e)
    {
        activated = false;
    }

    // Update is called once per frame
    void Update ()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
        if (timeout != 0f)
        {
            timeout -= Time.deltaTime;
            timeout = Mathf.Max(0f, timeout);
        }
    }

    void OnGUI()
    {
        if (!activated && lastInteract.AddSeconds(2) > DateTime.Now)
        {
            GUI.DrawTexture(new Rect(pos.x, pos.y, size.x, size.y), progressBarEmpty);
            GUI.DrawTexture(new Rect(pos.x, pos.y, size.x * Mathf.Clamp01(progress), size.y), progressBarFull);
        }
    }

    public bool IsActivated()
    {
        return activated;
    }

    public void Reset()
    {
        lastInteract = DateTime.Now.AddHours(-1);
        chrono = new Timer(activatedTime);
        chrono.Elapsed += resetUse;
        chrono.AutoReset = false;
        progress = 0f;
        activated = false;
        timeout = 0f;
        firstActivated = false;
    }
}
