﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;
using System;
using UnityEngine.Networking;

public class TimeSwitch : NetworkBehaviour, Interactable, Resetable
{

    bool use;
    private Timer chrono;
    private Animator animator;
    [SerializeField]
    private int activatedTime = 10000;
    private bool assigned = false;


    void Start()
    {
        animator = GetComponent<Animator>();
        use = false;
        chrono = new Timer(activatedTime);
        chrono.Elapsed += resetUse;
        chrono.AutoReset = false;
        ResetableUtils.InitReset(this);
    }

    void Update()
    {
        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }

    }

    void resetUse(object Source, ElapsedEventArgs e)
    {
        use = false;
    }

    void Interactable.OnInteract()
    {
        if (isServer)
            RpcOnInteract();
        else
            CmdOnInteract();
    }

    [Command]
    private void CmdOnInteract()
    {
        RpcOnInteract();
    }

    [ClientRpc]
    private void RpcOnInteract()
    {
        use = true;
        chrono.Interval = activatedTime;
        chrono.Enabled = true;
        animator.SetBool("On", true);
        StartCoroutine(ResetButtonAnimationState());
    }

    IEnumerator ResetButtonAnimationState()
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("On", false);
    }

    void Interactable.OnContinuousInteract()
    {
    }

    public bool returnUse()
    {
        return use;
    }

    public void Reset()
    {
        use = false;
        chrono = new Timer(activatedTime);
        chrono.Elapsed += resetUse;
        chrono.AutoReset = false;
    }
}
