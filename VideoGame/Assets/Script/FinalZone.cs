﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FinalZone : NetworkBehaviour, Resetable
{

    private int count = 0;
    [SerializeField]
    private int mapUnlock = 1;
    private bool winning;
    [SerializeField]
    private float delay = 3f;
    [SerializeField]
    private Animator doorAnimator;
    [SerializeField]
    private Animator elevatorAnimator;
    [SerializeField]
    private Collider[] doorColliders;

    void Start()
    {
        ResetableUtils.InitReset(this);
    }

    // Update is called once per frame
    void Update ()
    {
        if (winning)
        {
            delay -= Time.deltaTime;
            delay = Math.Max(delay, 0f);


            if (delay == 0f)
            {
                SettingInt setting = ((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.Level));
                setting.SetValue(Math.Max(setting.GetValue(), mapUnlock));
                setting.Save();
                ((NetManager)NetworkManager.singleton).Disconnect();
                winning = false;
            }
            return;
        }

	    if (isServer && count > 1)
            RpcOnWin();

    }

    [ClientRpc]
    private void RpcOnWin()
    {
        winning = true;
        foreach (Collider coll in doorColliders)
            coll.enabled = true;
        elevatorAnimator.SetBool("Open", false);
        doorAnimator.SetBool("Open", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        count++;
    }

    private void OnTriggerExit(Collider other)
    {
        count--;
        count = Math.Max(0, count);
    }

    public void Reset()
    {
        count = 0;
    }
}
