﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Inputs
{
    Avancer, Reculer, Gauche, Droite, Marcher, Sauter, Tirer, Console, Interagir, Changer, Special
}

public class InputManager {

    public static readonly string Prefix = "input";
    private Dictionary<Inputs, KeyCode> dictionnary = new Dictionary<Inputs, KeyCode>();

    public InputManager()
    {
        Init();
    }

    public void Init()
    {
        Dictionary<Inputs, KeyCode> settings = LoadSettings();
        
        foreach (var item in settings)
            dictionnary.Add(item.Key, item.Value);

        RegisterButton(Inputs.Avancer, KeyCode.Z);
        RegisterButton(Inputs.Reculer, KeyCode.S);
        RegisterButton(Inputs.Gauche, KeyCode.Q);
        RegisterButton(Inputs.Droite, KeyCode.D);
        RegisterButton(Inputs.Marcher, KeyCode.LeftShift);
        RegisterButton(Inputs.Sauter, KeyCode.Space);
        RegisterButton(Inputs.Tirer, KeyCode.Mouse0);
        RegisterButton(Inputs.Console, KeyCode.T);
        RegisterButton(Inputs.Interagir, KeyCode.E);
        RegisterButton(Inputs.Changer, KeyCode.F);
        RegisterButton(Inputs.Special, KeyCode.G);

        Save();
    }

    private void RegisterButton(Inputs input, KeyCode defaultButton)
    {
        if (!dictionnary.ContainsKey(input))
            dictionnary.Add(input, defaultButton);
    }


    private Dictionary<Inputs, KeyCode> LoadSettings()
    {
        Dictionary<Inputs, KeyCode> settings = new Dictionary<Inputs, KeyCode>();

        foreach (Inputs input in Enum.GetValues(typeof(Inputs)))
            if (SettingsHelper.Has(Prefix, input.ToString()))
                settings.Add(input, (KeyCode) Enum.Parse(typeof(KeyCode), SettingsHelper.GetString(Prefix, input.ToString())));

        return settings;
    }

    public void Save()
    {
        foreach (var item in dictionnary)
            SettingsHelper.Set(Prefix, item.Key.ToString(), item.Value.ToString());
    }

    public void SetButton(Inputs action, KeyCode button)
    {
        dictionnary[action] = button;
    }

    public KeyCode GetKeyCode(Inputs action)
    {
        return dictionnary[action];
    }

    public bool GetKey(Inputs action)
    {
        return Input.GetKey(dictionnary[action]);
    }

    public bool GetKeyDown(Inputs action)
    {
        return Input.GetKeyDown(dictionnary[action]);
    }

    public Dictionary<Inputs, KeyCode> GetInputs()
    {
        return this.dictionnary;
    }
}
