﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class CmdBind : ClientCommand {
    public CmdBind() : base("keybind")
    {
    }

    protected override bool CheckIntegrity(string[] args)
    {
        Debug.Log(args);
        return args.Length == 2
            && Enum.Parse(typeof(Inputs), args[0]) != null
            && Enum.Parse(typeof(KeyCode), args[1]) != null;
    }

    public override bool Run(string[] args)
    {
        if (!base.Run(args))
        {
            ConsoleManager.Instance.LocalDisplay("Usage: /keybind <touchName> <value>");
            return false;
        }
            
        MainGame.GetInputManager().GetInputs()[(Inputs) Enum.Parse(typeof(Inputs), args[0])] =  (KeyCode) Enum.Parse(typeof(KeyCode), args[1]);

        return true;
    }
}
