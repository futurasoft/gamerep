﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CmdParam : ClientCommand {
    public CmdParam() : base("param")
    {
    }

    protected override bool CheckIntegrity(string[] args)
    {
        if (args.Length != 2 || Enum.Parse(typeof(Settings), args[0]) == null)
            return false;

        Setting setting = MainGame.GetSettingManager().GetSetting((Settings)Enum.Parse(typeof(Settings), args[0]));

        if (setting is SettingInt)
        {
            int n;
            return int.TryParse(args[1], out n);
        }
        else if (setting is SettingFloat)
        {
            float f;
            return float.TryParse(args[1], out f);
        }
        else if (setting is SettingBool)
        {
            bool b;
            return bool.TryParse(args[1], out b);
        }
        else
        {
            int n2;
            Enum e = ((SettingEnum)setting).GetValue();

            if (int.TryParse(args[1], out n2))
                return n2 < Enum.GetValues(e.GetType()).Length;

            return Enum.Parse(e.GetType(), args[1]) != null;
        }
    }

    public override bool Run(string[] args)
    {
        if (!base.Run(args))
        {
            ConsoleManager.Instance.LocalDisplay("Usage: /param <paramName> <value>");
            return false;
        }

        Setting setting = MainGame.GetSettingManager().GetSetting((Settings)Enum.Parse(typeof(Settings), args[0]));

        if (setting is SettingInt)
            ((SettingInt)setting).SetValue(int.Parse(args[1]));
        else if (setting is SettingFloat)
            ((SettingFloat)setting).SetValue(float.Parse(args[1]));
        else if (setting is SettingBool)
            ((SettingBool)setting).SetValue(bool.Parse(args[1]));
        else
        {
            Enum e = ((SettingEnum)setting).GetValue();
            int n2;

            if (int.TryParse(args[1], out n2))
                e = ((Enum[]) Enum.GetValues(e.GetType()))[n2];
            else
                e = (Enum) Enum.Parse(e.GetType(), args[1]);

            ((SettingEnum)setting).SetValue(e);
        }

        ConsoleManager.Instance.LocalDisplay("You successfully changed this setting !");

        setting.Save();
        return true;
    }
}
