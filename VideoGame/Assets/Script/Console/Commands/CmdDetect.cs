﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CmdDetect : ServerCommand
{
    public CmdDetect() : base("detect", true)
    {
    }

    protected override bool CheckIntegrity(NetworkConnection target, GameObject sender, string[] args)
    {
        if (!base.CheckIntegrity(target, sender, args))
            return false;


        if (args.Length == 1 && (args[0] == "on" || args[0] == "off"))
            return true;

        sender.GetComponent<ConsoleManager>().TargetRpcDisplay(target, "Usage: /detect <off|on>");
        return false;
    }

    public override bool Run(NetworkConnection target, GameObject sender, string[] args)
    {
        if (!base.Run(target, sender, args))
            return false;

        Player.Detect = args[0] == "on";

        ConsoleManager.Instance.RpcDisplay(string.Format("The detection has been changed to {0} !", args[0]));
        return true;
    }
}
