﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CmdMinigame : ClientCommand
{

    public CmdMinigame() : base("minigame")
    {
    }

    public override bool Run(string[] args)
    {
        if (!base.Run(args))
            return false;

        GameObject.Find("Canvas").transform.FindChild("MiniGame").GetComponent<Minigame>().Activate();
        ConsoleManager.Instance.LocalDisplay("You spawn the end minigame !");
        return true;
    }
}
