﻿using System;

public class CheatIsNotEnabledException : Exception {
    public CheatIsNotEnabledException() : base("This command need cheat mode to be activated !")
    {

    }
}


public class CommandNotFoundException : Exception
{
    public CommandNotFoundException() : base("This command doesn't exist !")
    {

    }
}