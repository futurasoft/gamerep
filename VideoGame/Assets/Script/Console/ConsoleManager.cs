﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ConsoleManager : NetworkBehaviour {

    private CommandManager commandManager = new CommandManager();

    private ConsoleHandler consoleHandler;
    public ConsoleHandler ConsoleHandler { get { return this.consoleHandler; } }

    public static ConsoleManager Instance { get; private set; }

    void Start()
    {
        consoleHandler = GameObject.Find("Console").GetComponent<ConsoleHandler>();
        Instance = this;
    }

    public override void OnStartLocalPlayer()
    {
        consoleHandler = GameObject.Find("Console").GetComponent<ConsoleHandler>();
        consoleHandler.consoleManager = this;
        base.OnStartLocalPlayer();
    }

    public void HandleInput(string input)
    {
        if (input.IndexOf("/") == 0)
        {
            int firstSpace = input.IndexOf(' ') == -1 ? input.Length - 1 : input.IndexOf(' ') - 1;

            try
            {
                string cmd = input.Substring(1, firstSpace);
                
                string[] args;

                if (firstSpace == input.Length - 1)
                    args = new string[0];
                else
                    args = input.Substring(firstSpace + 2, input.Length - firstSpace - 2).Split(' ');

                Command cmdObj = commandManager.GetCommand(cmd);

                if (cmdObj == null)
                    throw new CommandNotFoundException();

                if (cmdObj is ServerCommand)
                {
                    if (isServer)
                        ((ServerCommand)cmdObj).Run(connectionToClient, Player.LocalPlayer.gameObject, args);
                    else
                        CmdHandleCommand(cmd, Player.LocalPlayer.gameObject, args);
                }
                    
                else
                    ((ClientCommand)cmdObj).Run(args);
            }
            catch (Exception e)
            {
                LocalDisplay(e.Message);
            }
        }
        else
            CmdDisplay(Player.LocalPlayer.PlayerName, input);
    }

    [Command]
    private void CmdHandleCommand(string cmd, GameObject sender, string[] args)
    {
        ((ServerCommand)commandManager.GetCommand(cmd)).Run(connectionToClient, sender, args);
    }

    public void CmdDisplay(string name, string input)
    {
        CmdDisplay(name + ": " + input);
    }

    [Command]
    public void CmdDisplay(string input)
    {
        RpcDisplay(input);
    }

    public void Display(string name, string input)
    {
        RpcDisplay(name + ": " + input);
    }

    [ClientRpc]
    public void RpcDisplay(string input)
    {
        LocalDisplay(input);
    }

    [TargetRpc]
    public void TargetRpcDisplay(NetworkConnection target, String input)
    {
        LocalDisplay(input);
    }

    public void LocalDisplay(string name, string input)
    {
        LocalDisplay(name + ": " + input);
    }

    public void LocalDisplay(string input)
    {
        consoleHandler.AddTextLine(input);
    }
}
