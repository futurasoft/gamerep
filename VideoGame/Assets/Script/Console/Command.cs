﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Command
{
    private string name;
    public string Name { get { return name; } }

    public Command(string name)
    {
        this.name = name.ToLower();
    }
}

public abstract class ServerCommand : Command
{
    private bool needCheat;
    public bool NeedCheat { get { return needCheat; } }

    public ServerCommand(string name, bool needCheat) : base(name)
    {
        this.needCheat = needCheat;
    }

    // extend this if you want to have more in depth checks for your command
    protected virtual bool CheckIntegrity(NetworkConnection target, GameObject sender, string[] args)
    {
        if (!NeedCheat || ((SettingBool)MainGame.GetSettingManager().GetSetting(Settings.Cheating)).GetValue())
            return true;

        sender.GetComponent<ConsoleManager>().TargetRpcDisplay(target, "The server doesn't have cheats activated !");

        return false;
    }

    // extend this to have the action of your command
    public virtual bool Run(NetworkConnection target, GameObject sender,  string[] args)
    {
        return CheckIntegrity(target, sender, args);
    }

}

public abstract class ClientCommand : Command
{
    public ClientCommand(string name) : base(name)
    {
    }

    // extend this if you want to have more in depth checks for your command
    protected virtual bool CheckIntegrity(string[] args)
    {
        return true;
    }

    // extend this to have the action of your command
    public virtual bool Run(string[] args)
    {
        return CheckIntegrity(args);
    }
}
