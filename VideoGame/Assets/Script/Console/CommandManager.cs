﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CommandManager
{
    [SerializeField]
    private HashSet<Command> commands = new HashSet<Command>()
    {
        new CmdMinigame(),
        new CmdBind(),
        new CmdParam(),
        new CmdDetect()
    };


    public CommandManager()
    {
    }

    public Command GetCommand(string name)
    {
        name = name.ToLower();

        foreach (Command cmd in commands)
            if (cmd.Name == name)
                return cmd;

        return null;
    }
}
