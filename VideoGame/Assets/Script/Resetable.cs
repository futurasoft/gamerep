﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Resetable
{
    void Reset();
}

public class ResetableUtils
{

    private static List<Resetable> resets = new List<Resetable>();

    public static void InitReset(Resetable resetable)
    {
        resets.Add(resetable);
    }

    public static void ResetScene()
    {
        foreach (Resetable reset in resets)
            reset.Reset();
    }

    public static void Clear()
    {
        resets.Clear();
    }
}
