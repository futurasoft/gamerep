﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class ActiveGUI
{
    public static HashSet<IDesactivable> set = new HashSet<IDesactivable>();

    public static void Desactivate()
    {
        if (set.Last() != null)
            set.Last().Desactivate();
    }
}

public interface IDesactivable
{
    void Desactivate();
}
