﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ZoneEndTriggerSound : NetworkBehaviour, Resetable
{
    [SerializeField]
    private VoiceoverTrigger trigger;

    private bool hasTrigger = false;

    public void Reset()
    {
        hasTrigger = false;
    }

    void Start()
    {
        ResetableUtils.InitReset(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Player>() != null && Receptor.IsWinning() && isServer && !hasTrigger)
        {
            trigger.Trigger();
            hasTrigger = true;
        }
            
    }

}
