﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class VoiceoverManager : NetworkBehaviour, Resetable
{
    private int i = 0;
    private VoiceoverTrigger trigger = null;
    private float totalTime = 0f;
    private AudioSource audioSource;
    private Text text;
    private bool assigned = false;


    // Use this for initialization
    void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        text = GameObject.Find("Canvas").transform.FindChild("Subtittle").GetComponent<Text>();
        ResetableUtils.InitReset(this);
	}
	
	// Update is called once per frame
	void Update ()
    {
        float volume = ((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.VoiceVolume)).GetValue() / 100.0f;
        float mastervolume = ((SettingInt)MainGame.GetSettingManager().GetSetting(Settings.MasterVolume)).GetValue() / 100.0f;
        audioSource.volume = volume * mastervolume;

        if (trigger != null)
        {
            totalTime += Time.deltaTime;
            if (i + 1 < trigger.timings.GetLength(0) && trigger.timings[i + 1] < totalTime)
                i++;
            text.text = trigger.sentences[i];

            if (totalTime >= trigger.endTime)
            {
                Reset();
            }
        }
        else
            text.text = "";

        if (!isServer && !isLocalPlayer && !assigned)
        {
            Player.LocalPlayer.AssignClientAuthority(netId);
            assigned = true;
        }
    }

    public void Trigger(GameObject trigger)
    {
        if (isServer)
            RpcTrigger(trigger);
        else
            CmdTrigger(trigger);
    }

    public void TriggerLocal(GameObject trigger)
    {
        Reset();
        this.trigger = trigger.GetComponent<VoiceoverTrigger>();
        audioSource.clip = this.trigger.clip;
        audioSource.Play();
    }

    [Command]
    private void CmdTrigger(GameObject trigger)
    {
        RpcTrigger(trigger);
    }

    [ClientRpc]
    private void RpcTrigger(GameObject trigger)
    {
        TriggerLocal(trigger);
    }

    public void Reset()
    {
        audioSource.Stop();
        trigger = null;
        i = 0;
        totalTime = 0f;
    }
}
