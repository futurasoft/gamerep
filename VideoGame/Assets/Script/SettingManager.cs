﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Settings
{
    Cheating, Level, MasterVolume, EffectVolume, MusicVolume, QualityLevel, VoiceVolume
}

public class SettingManager {

    private Dictionary<Settings, Setting> dictionnary = new Dictionary<Settings, Setting>();

    public SettingManager()
    {
        Init();
    }

    public void Init()
    {
        RegisterSetting(new SettingBool("Enable cheat", Settings.Cheating, false));
        RegisterSetting(new SettingInt("Son maitre", Settings.MasterVolume, 50 , 0, 100));
        RegisterSetting(new SettingInt("Son des effets", Settings.EffectVolume, 50, 0, 100));
        RegisterSetting(new SettingInt("Son des musiques", Settings.MusicVolume, 50, 0, 100));
        RegisterSetting(new SettingInt("Son des voix", Settings.VoiceVolume, 50, 0, 100));
        RegisterSetting(new SettingInt("Level", Settings.Level, 0, 0, 4, true));
        RegisterSetting(new SettingQuality());
        // ... (Go check in Setting.cs for more setting type)

        Save();
    }

    public void Save()
    {
        foreach (KeyValuePair<Settings, Setting> pair in dictionnary)
            pair.Value.Save();
    }

    public Setting GetSetting(Settings setting)
    {
        return dictionnary[setting];
    }

    private void RegisterSetting(Setting setting)
    {
        if (!dictionnary.ContainsKey(setting.GetSettings()))
        {
            dictionnary.Add(setting.GetSettings(), setting);
            setting.Load();
        }
    }

    public Dictionary<Settings, Setting> GetSettings()
    {
        return this.dictionnary;
    }
}
