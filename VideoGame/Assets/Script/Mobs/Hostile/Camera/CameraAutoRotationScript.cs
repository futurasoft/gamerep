﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraAutoRotationScript : NetworkBehaviour, Resetable, IStunnable
{
    [SerializeField]
    private float angle = 30f;

    [SerializeField]
    private bool standardDirection = true;

    [SerializeField]
    private float rotationSpeed = 15f;
    private bool revert = false;
    private float _angle = 0.51f;

    private bool stopped = false;
    private GameObject detection;

    [SerializeField]
    private float stunTime = 4f;
    private float _stunTime;

    [SerializeField]
    private bool canBeStun = true;


    // Use this for initialization
    void Start () {
        ResetableUtils.InitReset(this);
        detection = transform.FindChild("Hitbox Detection").gameObject;

        if (angle == 360f)
            Debug.Log("[WARN]Mob->Hostile->Camera->" + ToString() + ": You setted 'start' to 360, the camera will rotate infinitely");
    }

    // Update is called once per frame
    void Update () {
        //Debug.Log(transform.rotation.eulerAngles.y);

        if (!isServer)
            return;

        if (stopped)
        {
            _stunTime -= Time.deltaTime;
            _stunTime = Mathf.Max(_stunTime, 0f);

            if (_stunTime == 0f)
            {
                stopped = false;
                detection.SetActive(true);
            }

            return;
        }

        float currentAngle = rotationSpeed * Time.deltaTime;

        if (!revert)
            currentAngle = Math.Min(angle - _angle, currentAngle);
        else
            currentAngle = Math.Min(_angle, currentAngle);

        if (revert)
            currentAngle *= -1;

        _angle += currentAngle;

        if (!standardDirection)
            currentAngle *= -1;

        transform.Rotate(0, currentAngle, 0);

        if (_angle <= 0.5)
            revert = !revert;
        else if (_angle >= angle - 0.5)
            revert = !revert;
    }

    public void Reset()
    {
        if (standardDirection)
            transform.Rotate(0, -angle, 0);
        else
            transform.Rotate(0, angle, 0);
        stopped = false;
        _angle = 0f;
        revert = false;
        detection.SetActive(true);
        _stunTime = 0f;
    }

    public void Stun()
    {
        if (!canBeStun)
            return;

        detection.SetActive(false);
        stopped = true;
        _stunTime = stunTime;
    }
}