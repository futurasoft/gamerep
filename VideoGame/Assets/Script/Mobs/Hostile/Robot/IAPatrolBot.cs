﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class IAPatrolBot : NetworkBehaviour, Resetable, IStunnable
{
    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private float speed = 3f;

    [SerializeField]
    private float stunTime = 4f;
    private float _stunTime = 1f;

    [SerializeField]
    private bool canBeStun = true;

    private Transform[] waypoints;
    [SerializeField]
    private int currentWaypointIndex;

    private Vector3 startPos;
    private Quaternion startRot;

    private bool stopped = true;
    private GameObject detection;

    [SerializeField]
    private float rotationSpeed = 6.0f;
    private int initialWaypoint;

    void Start()
    {
        startPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        startRot = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        detection = transform.Find("Hitbox Detection").gameObject;
        initialWaypoint = currentWaypointIndex;
        ResetableUtils.InitReset(this);

        waypoints = new Transform[parent.transform.childCount];
        for (int i = 0; i < parent.transform.childCount; i++)
            waypoints[i] = parent.transform.GetChild(i);

        if (waypoints.Length == 0)
            Debug.Log("[ERR]Mob->Hostile->Robot->" + ToString() + ": No waypoint defined.");
    }

    void Update()
    {
        if (!isServer)
            return;

        if (stopped)
        {
            _stunTime -= Time.deltaTime;
            _stunTime = Mathf.Max(_stunTime, 0f);

            if (_stunTime == 0f)
            {
                stopped = false;
                detection.SetActive(true);
            }

            return;
        }

        Vector3 pos = waypoints[currentWaypointIndex].position;
        pos.Set(pos.x, transform.position.y, pos.z);

        if (Vector3.Distance(transform.position, pos) <= 0.01)
        {
            if (currentWaypointIndex < waypoints.Length - 1)
                currentWaypointIndex++;
            else
                currentWaypointIndex = 0;
        }
        else
        {
            Vector3 oldPosition = transform.position;
            transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
            var targetRotation = Quaternion.LookRotation(transform.position - oldPosition);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
    }

    public void Reset()
    {
        if (!isServer)
            return;

        transform.position = startPos;
        transform.rotation = startRot;
        currentWaypointIndex = initialWaypoint;
        stopped = true;
        detection.SetActive(true);
        _stunTime = 1f;
    }

    public void Stun()
    {
        if (!canBeStun)
            return;

        detection.SetActive(false);
        stopped = true;
        _stunTime = stunTime;
    }
}