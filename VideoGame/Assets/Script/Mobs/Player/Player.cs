﻿using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public class Player : NetworkBehaviour, Resetable
{

    private static Player localPlayer;
    public static Player LocalPlayer { get { return localPlayer; } }

    private string playerName;
    public string PlayerName { get { return this.playerName; } }

    private bool stopMove = false;
    public bool StopMove { get { return this.stopMove; } set { stopMove = value; } }

    private static bool haveGun = true;
    public static bool HaveGun { get { return haveGun; } set { haveGun = value; } }

    private static bool haveCracker = true;
    public static bool HaveCracker { get { return haveCracker; } set { haveCracker = value; } }

    private static bool haveCard = false;
    public static bool HaveCard { get { return haveCard; } set { haveCard = value; } }

    private static bool haveGunDefault = false;
    public static bool HaveGunDefault { get { return haveGunDefault; } set { haveGunDefault = value; } }

    private static bool haveCrackerDefault = false;
    public static bool HaveCrackerDefault { get { return haveCrackerDefault; } set { haveCrackerDefault = value; } }

    private static bool detect = true;
    public static bool Detect { get { return detect; } set { detect = value; } }

    [SerializeField]
    private int layerDetector = 9;
    private GameObject pauseMenu;
    private static List<GameObject> detectors = new List<GameObject>();

    private GameObject diedText;
    private GameObject waitingMenu;

    private Vector3 startPos;
    private Quaternion startRot;

    private GameObject cam;

    public float timeout = 0f;
    public bool choosen = true;
    public Player otherSoloPlayer = null;

    private Animator animator;
    private GameObject gun;
    private bool resetting = false;

    private FadeManager fadeManager;

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        if (localPlayer == null)
            localPlayer = this;

        playerName = Environment.UserName;

        if (((NetManager)NetworkManager.singleton).isSolo && otherSoloPlayer == null)
            CmdSpawnSecondPlayer();
    }

    [Command]
    public void CmdSpawnSecondPlayer()
    {
        GameObject other = Instantiate(
            NetworkManager.singleton.playerPrefab,
            NetworkManager.singleton.startPositions[1].position,
            NetworkManager.singleton.startPositions[1].rotation);
    
        other.GetComponent<Player>().otherSoloPlayer = this;
        otherSoloPlayer = other.GetComponent<Player>();

        NetworkServer.AddPlayerForConnection(connectionToClient, other, 1);
    }

    void Start ()
    {

        startPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        startRot = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        cam = transform.FindChild("World Camera").gameObject;
        animator = GetComponent<Animator>();
        gun = transform.FindChild("Stun Gun").gameObject;
        detectors.Clear();

        ResetableUtils.InitReset(this);

        if (!isLocalPlayer)
            return;
        
        waitingMenu = GameObject.Find("Canvas").transform.FindChild("Waiting Menu").gameObject;
        pauseMenu = GameObject.Find("Canvas").transform.FindChild("Menu Pause").gameObject;
        diedText = GameObject.Find("Canvas").transform.FindChild("DiedText").gameObject;
        diedText.GetComponent<Text>().enabled = true;
        diedText.SetActive(false);
        haveGun = haveGunDefault;
        haveCracker = haveCrackerDefault;

        fadeManager = (FadeManager)GameObject.Find("Canvas").GetComponent(typeof(FadeManager));
    }

    public GameObject GetCamera()
    {
        return this.cam;
    }

    void Update ()
    {
        UpdateDetection();
        animator.SetBool("Weapon in hand", haveGun);
        gun.SetActive(haveGun);

        if (!isLocalPlayer || !choosen)
            return;

        timeout -= Time.deltaTime;
        timeout = Mathf.Max(timeout, 0f);

        if (!((NetManager)NetworkManager.singleton).isSolo)
            if (NetworkManager.singleton.numPlayers == 1)
            {
                waitingMenu.GetComponent<Waiting>().Enable(cam); 
                stopMove = true;
            }
            else
            {
                waitingMenu.GetComponent<Waiting>().Disable(cam);
                if (ActiveGUI.set.Count == 0)
                    stopMove = false;
            } 
        else
            waitingMenu.GetComponent<Waiting>().Disable(cam);

        if (MainGame.GetInputManager().GetKeyDown(Inputs.Console))
        {
            if (ActiveGUI.set.Count == 0)
            {
                stopMove = true;
                GetComponent<ConsoleManager>().ConsoleHandler.Activate(gameObject);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (ActiveGUI.set.Count == 0)
            {
                stopMove = true;
                pauseMenu.GetComponent<PauseMenu>().Activate(gameObject, cam);
            }
            else
            {
                ActiveGUI.Desactivate();
            }

        }

        if (((NetManager)NetworkManager.singleton).isSolo && !stopMove && timeout == 0 && MainGame.GetInputManager().GetKeyDown(Inputs.Changer))
        {
            GetComponent<DesactivateLocalFeatures>().Disable();
            otherSoloPlayer.GetComponent<DesactivateLocalFeatures>().Enable();
            localPlayer = otherSoloPlayer;
        }

        Interactable interact = null;

        if (MainGame.GetInputManager().GetKey(Inputs.Interagir) || MainGame.GetInputManager().GetKeyDown(Inputs.Interagir))
            interact = TryInteract();

        if (interact == null)
            return;

        if (MainGame.GetInputManager().GetKeyDown(Inputs.Interagir))
            interact.OnInteract();

        if (MainGame.GetInputManager().GetKey(Inputs.Interagir))
            interact.OnContinuousInteract();
    }

    public void AssignClientAuthority(NetworkInstanceId netid)
    {
        CmdAssignClientAuthority(netid, GetComponent<NetworkIdentity>());
    }

    [Command]
    private void CmdAssignClientAuthority(NetworkInstanceId netid, NetworkIdentity identity)
    {
        NetworkServer.FindLocalObject(netid).GetComponent<NetworkIdentity>().AssignClientAuthority(identity.connectionToClient);
    }

    public void StartMove()
    {
        stopMove = false;
    }

    private void UpdateDetection()
    {
        if (!isServer || resetting || !choosen || !detect)
            return;

        foreach (GameObject other in detectors)
        {
            int layerMask = 1 << layerDetector;
            layerMask = ~layerMask;
            RaycastHit hit;
            Transform detectionStart = other.transform.parent.FindChild("Detection Start");
            Vector3 dir = transform.FindChild("Stun Gun").position - detectionStart.position;
            Ray ray = new Ray(detectionStart.position, dir);

            if (Physics.Raycast(ray, out hit, Camera.main.farClipPlane, layerMask) && hit.transform.gameObject == gameObject)
            {
                resetting = true;
                if (((NetManager)NetworkManager.singleton).isSolo)
                    ResetableUtils.ResetScene();
                else
                    RpcReset();
            }

        }
    }

    [ClientRpc]
    private void RpcReset()
    {
        ResetableUtils.ResetScene();
    }

    private Interactable TryInteract()
    {
        RaycastHit hit;

        int layerMask = 1 << 9;
        layerMask = ~layerMask;

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, Mathf.Infinity, layerMask))
        {

            return ((Interactable)hit.transform.GetComponent(typeof(Interactable)));
        }
        return null;
    }

    void OnTriggerEnter(Collider other)
    {
        if (isServer && other.gameObject.tag == "Detection")
        {
            detectors.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (isServer && other.gameObject.tag == "Detection")
        {
            detectors.Remove(other.gameObject);
        }
    }

    public void Reset()
    {
        if (!isLocalPlayer)
            return;

        if (choosen)
            fadeManager.Fade(true, 0.6f);

        StartCoroutine(WaitForTP());

        resetting = false;
    }

    public IEnumerator WaitForTP()
    {
        yield return new WaitForSeconds(0.6f);
        detectors.Clear();
        transform.position = startPos;
        transform.rotation = startRot;
        diedText.SetActive(true);
        haveCracker = haveCrackerDefault;
        haveGun = haveGunDefault;
        fadeManager.Fade(false, 0.6f);
    }
}
