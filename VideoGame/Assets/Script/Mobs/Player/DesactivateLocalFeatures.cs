﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DesactivateLocalFeatures : NetworkBehaviour
{
    [SerializeField]
    private Vector3 posCamPlayer1;
    [SerializeField]
    private Vector3 posCamPlayer2;
    [SerializeField]
    private GameObject model1;
    [SerializeField]
    private GameObject model2;
    private GameObject model;
    [SerializeField]
    private Vector3 posGun1;
    [SerializeField]
    private Vector3 posGun2;
    [SerializeField]
    private Vector3 posGunCam1;
    [SerializeField]
    private Vector3 posGunCam2;

    void Start ()
    {
        
        NetManager netManager = (NetManager)NetworkManager.singleton;
        GameObject camObj = transform.FindChild("World Camera").gameObject;
        

        if (!isLocalPlayer)
        {
            if (netManager.isHost && netManager.firstSpawned)
            {
                camObj.transform.localPosition = posCamPlayer2;
                model = Instantiate(model2, Vector3.zero, transform.rotation, transform);
                transform.FindChild("Stun Gun").localPosition = posGun2;
                transform.FindChild("Gun Camera").localPosition = posGunCam2;
            }
            else
            {
                camObj.transform.localPosition = posCamPlayer1;
                model =  Instantiate(model1, Vector3.zero, transform.rotation, transform);
                transform.FindChild("Stun Gun").localPosition = posGun1;
                transform.FindChild("Gun Camera").localPosition = posGunCam1;
            }

            Disable();
        }
        else
        {
            if (netManager.isHost && !netManager.firstSpawned)
            {
                camObj.transform.localPosition = posCamPlayer1;
                model = Instantiate(model1, Vector3.zero, transform.rotation, transform);
                transform.FindChild("Stun Gun").localPosition = posGun1;
                transform.FindChild("Gun Camera").localPosition = posGunCam1;
            }
            else
            {
                camObj.transform.localPosition = posCamPlayer2;
                model = Instantiate(model2, Vector3.zero, transform.rotation, transform);
                transform.FindChild("Stun Gun").localPosition = posGun2;
                transform.FindChild("Gun Camera").localPosition = posGunCam2;
            }

            if (!netManager.isSolo)
                Enable();
            else if (!netManager.firstSpawned)
                Enable();
            else
                Disable();
                
        }

        GetComponent<Animator>().avatar = model.GetComponent<Animator>().avatar;
        camObj.transform.localScale = Vector3.one;
        model.transform.localScale = Vector3.one;
        GetComponent<CharacterController>().center = model.GetComponent<CapsuleCollider>().center;
        GetComponent<CharacterController>().height = model.GetComponent<CapsuleCollider>().height;
        netManager.firstSpawned = true;
        model.transform.localPosition = Vector3.zero;

        
    }
	
    public void SetLayer(Transform transform, int layer)
    {
        transform.gameObject.layer = layer;

        for (int i = 0; i < transform.childCount; i++)
            SetLayer(transform.GetChild(i), layer);
    }

    public void Disable()
    {
        GameObject camObj = transform.FindChild("World Camera").gameObject;
        camObj.transform.FindChild("UI Camera").gameObject.GetComponent<Camera>().enabled = false;
        camObj.gameObject.GetComponent<Camera>().enabled = false;
        camObj.GetComponent<AudioListener>().enabled = false;
        transform.FindChild("Gun Camera").GetComponent<Camera>().enabled = false;
        SetLayer(transform.FindChild("Stun Gun"), 0);
        GetComponent<Player>().StopMove = true;
        GetComponent<Player>().choosen = false;
        GetComponent<Player>().timeout = 0.2f;

        for (int i = 0; i < model.transform.childCount; i++)
            model.transform.GetChild(i).gameObject.SetActive(true);
    }

    public void Enable()
    {
        GameObject camObj = transform.FindChild("World Camera").gameObject;
        GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = camObj.transform.FindChild("UI Camera").gameObject.GetComponent<Camera>();
        camObj.transform.FindChild("UI Camera").gameObject.GetComponent<Camera>().enabled = true;
        camObj.gameObject.GetComponent<Camera>().enabled = true;
        camObj.GetComponent<AudioListener>().enabled = true;
        transform.FindChild("Gun Camera").GetComponent<Camera>().enabled = true;
        SetLayer(transform.FindChild("Stun Gun"), 8);
        GetComponent<Player>().StopMove = false;
        GetComponent<Player>().choosen = true;
        GetComponent<Player>().timeout = 0.2f;

        for (int i = 0; i < model.transform.childCount; i++)
            model.transform.GetChild(i).gameObject.SetActive(false);
    }

    private bool fix = false;

    public void Update()
    {
        if (Player.HaveGun && !fix)
        {
            fix = true;
            GetComponent<CharacterController>().height += 0.25f;
            model.GetComponent<CapsuleCollider>().height += 0.25f;
        }
        else if (!Player.HaveGun && fix)
        {
            fix = false;
            GetComponent<CharacterController>().height -= 0.25f;
            model.GetComponent<CapsuleCollider>().height -= 0.25f;

        }
    }
}
