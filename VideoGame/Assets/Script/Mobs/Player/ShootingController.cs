﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;
using UnityEngine.UI;
using LightningBolt.LightningBolt;

public class ShootingController : NetworkBehaviour, Resetable
{
    [SerializeField]
    private GameObject Bullet_Emitter;
    [SerializeField]
    private GameObject Bullet;
    [SerializeField] 
    private float Bullet_Forward_Force;

    [SerializeField]
    private AudioClip SoundTir;

    private float _cooldown = 0;

    [SerializeField]
    private float cooldown = 1;
    private Slider bar;
    private LightningBoltScript shooter;

    void Start()
    {
        bar = GameObject.Find("Canvas").transform.FindChild("ShootBar").GetComponent<Slider>();
        bar.gameObject.SetActive(false);
        shooter = GetComponent<LightningBoltScript>();
    }

    void Update()
    {
        if (!isLocalPlayer || !Player.HaveGun || !GetComponent<Player>().choosen)
            return;

        _cooldown -= Time.deltaTime;
        _cooldown = Math.Max(0, _cooldown);

        bar.value = Mathf.Clamp01(_cooldown / cooldown);

        if (_cooldown != 0f)
            bar.gameObject.SetActive(true);
        else
            bar.gameObject.SetActive(false);

        if (!GetComponent<Player>().StopMove && MainGame.GetInputManager().GetKey(Inputs.Tirer) && _cooldown == 0)
        {
            _cooldown = cooldown;
            GetComponent<AudioSource>().PlayOneShot(SoundTir);

            Transform cam = GetComponent<Player>().GetCamera().transform;
            RaycastHit hit;
            Ray ray = cam.GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            int layerMask = 1 << 9;
            layerMask = ~layerMask;

            if (Physics.Raycast(cam.position, cam.forward, out hit, Mathf.Infinity, layerMask))
            {
                IStunnable stunnable = ((IStunnable)hit.transform.GetComponent(typeof(IStunnable)));

                if (stunnable != null)
                {
                    if (isServer)
                        stunnable.Stun();
                    else
                        CmdStun(hit.transform.gameObject);
                }
            }

            if (isServer)
                RpcShoot(hit.point);
            else
                CmdShoot(hit.point);

        }
    }

    [Command]
    private void CmdStun(GameObject gameobject)
    {
        IStunnable stunnable = ((IStunnable)gameobject.GetComponent(typeof(IStunnable)));
        stunnable.Stun();
    }

    [Command]
    private void CmdShoot(Vector3 dest)
    {
        RpcShoot(dest);
    }

    [ClientRpc]
    private void RpcShoot(Vector3 dest)
    {
        shooter.StartPosition = Bullet_Emitter.transform.position;
        shooter.EndPosition = dest;
        shooter.Trigger();
    }

    public void Reset()
    {
        _cooldown = 0;
    }

}
