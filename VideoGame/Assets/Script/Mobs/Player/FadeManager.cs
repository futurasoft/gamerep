﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FadeManager : MonoBehaviour {

	public static FadeManager Instance { get; set; }
    [SerializeField]
    private Image fadeImage;
    [SerializeField]
    private Color color;
    private bool isInTransition, showing;
    private float transition;
    private float duration;

    private void Awake()
    {
        Instance = this;
    }

    public void Fade(bool showing, float duration)
    {
        this.showing = showing;
        this.duration = duration;
        isInTransition = true;
        transition = (this.showing) ? 0 : 1;
    }

    private void Update()
    {

        if (!isInTransition)
            return;

        transition += (showing) ? Time.deltaTime * (1 / duration) : - Time.deltaTime * (1/duration);
        fadeImage.color = Color.Lerp(new Color(1, 1, 1, 0), Color.white, transition);

        if (transition > 1 || transition < 0)
            isInTransition = false;
    }
}
