﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AutoTilingMaterial : MonoBehaviour
{

    [SerializeField]
    public Vector2 ratio = new Vector2(1, 1);
    [SerializeField]
    public Texture2D texture;

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update()
    {
        GetComponent<Renderer>().sharedMaterial = new Material(Shader.Find("Diffuse"));
        GetComponent<Renderer>().sharedMaterial.SetTextureScale("_MainTex", new Vector2(ratio.x * transform.lossyScale.x, ratio.y * transform.lossyScale.y));
        GetComponent<Renderer>().material.mainTexture = texture;
    }
}
