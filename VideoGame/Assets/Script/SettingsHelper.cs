﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHelper {

    public static bool Has(string prefix, string key)
    {
        return PlayerPrefs.HasKey(prefix + ":" + key);
    }

	public static void Set(string prefix, string key, int value)
    {
        PlayerPrefs.SetInt(prefix + ":" + key, value);
    }

    public static void Set(string prefix, string key, float value)
    {
        PlayerPrefs.SetFloat(prefix + ":" + key, value);
    }

    public static void Set(string prefix, string key, string value)
    {
        PlayerPrefs.SetString(prefix + ":" + key, value);
    }

    public static float GetFloat(string prefix, string key)
    {
        return PlayerPrefs.GetFloat(prefix + ":" + key);
    }

    public static string GetString(string prefix, string key)
    {
        return PlayerPrefs.GetString(prefix + ":" + key);
    }

    public static int GetInt(string prefix, string key)
    {
        return PlayerPrefs.GetInt(prefix + ":" + key);
    }
}
