# Configuring your git project

## Configuring UnityYAMLMerge

### Bind the merge tool

Type `git config --global --edit` in your terminal, then add the following to your config file:

```
[merge]
    tool = unityyamlmerge
[mergetool "unityyamlmerge"]
    trustExitCode = false
    keepTemporaries = true
    keepBackup = false
    path = <Unity Install Location>/Unity/Editor/UnityYAMLMerge.exe
    cmd  = \"<Unity Install Location>/Unity/Editor/Data/Tools/UnityYAMLMerge.exe\" merge -p "$BASE" "$REMOTE" "$LOCAL" "$MERGED"
```
* Don't forget to change the path according to your Unity installation !

### Setup a fallback

I recommand you to install [KDiff3](http://kdiff3.sourceforge.net/).

Then find the file "mergespecfile.txt" in <Unity Install Location>/Editor/Data/Tools.

In the first lines you `YouFallbackMergeToolForScenesHere.exe` by your fallback.
For KDiff3, It will be something like `KDiff3\kdiff3.exe`.
Then we will add default fallback for all files with this line `* use “%programs%/YouFallbackMergeToolForScenesHere.exe” “%b” “%l” “%r” -o “%d”`.


## Install git-lfs

```
git lfs install
```
