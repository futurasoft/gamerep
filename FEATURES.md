### What's this file? ###
This file list all feature to implement to the project in a priority order, the highest one are just below and the lowest one is at the end of this document. When a feature has been implemented, you should remove it from this list.

You should place tags after the feature's name to know it's actual state:
* feature [BF] -------> Bug fixing state
* feature [FS] -------> Finished soon state
* feature [ID] -------> In dev state
* feature [NS] -------> Not started yet state

You should also place a tag with your name to indicate who is working on this functionnality.
* Mr Freezex 	-------> [MFX]
* Raph 		-------> [RAF]
* Und		-------> [UND]
* _________	-------> [___]

You could put the files which are used for these functionnality to prevent conflicts. Don't forget to separate them with a ";" (without quotes).

### Here is an exemple ###
* Creation of README.md 						[FS]		[UND]		README.md

### Features to implement ###
* Creation of the project.						[NS]
* Creation of the multiplayer system.					[NS]
* Creation of the core.							[NS]
* Creation of the elevator system between levels.			[NS]
* Creation of energy ball system.					[NS]
* Creation of the short range teleportation system.			[NS]
* Creation of the 2D minigame system to unlock end level elevator.	[NS]
