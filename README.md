﻿# README THIS project#

### What is this project? ###

* This is a project for the exam
* Direct link to find it:https://YourPseudo@bitbucket.org/futurasoft/gamerep.git

### Motivation ###

* The fun
* The money
* The fame
* Absolutely not the school mark

### Version: ###

* Release

### How do I get set up? ###

* Get the code
* Get Unity
* Get a code editor
* Use unity and apply what you learnt
* Then launch and enjoy!

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### How this project is organised? ###

* Red line is explained in the wiki.
* Ask to repo owner or admin for further details.

### Contributors ###

* Pierre-Emmanuel 'Und' Patry
* Arthur 'Mr_Freezex' Outhenin-Chalandre
* Raphaël 'Raphis' Heimann
* Guillaume 'Dyzing' Trem

### License ###

Undertermined yet, we should work on that soon.